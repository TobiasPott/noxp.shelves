### What is this repository for? ###

The NoXP.Shelves package is a rebuild of the shelf-system from Autodesk Maya which provides faster access to tools and actions normally located in the programs menu.

NoXP.Shelves reproduces the basic features and provides access to all objects and components which are part of the Unity default menu. Which allows creation of new game objects and assignment of components to game objects.

The shelf-system is designed to be user-extendible by creating custom sets of xml configuration files. 
These allow a user to either define shelves for his own set of menu items (e.g. custom scripts which show up in the "Component" menu) or actions (e.g. editor extensions hooked up into the interface) and calls to methods of internal classes or libraries.


### How do I get set up? ###

| Steps | |
| ------|------|
| Download this repository to your local drive |
| Create a new Unity project (or open an existing one) |
| Create a folder named "Editor" (if it does not exist) | ![Project View](Documentation/Doc_ProjectView.jpg) |
| Create a folder named "NoXP.Shelves" inside the "Editor" folder |
| Copy all files from this repository in the "NoXP.Shelves" folder |
| Wait for Unity to finish it's compilation of the new scripts and assets |
| Click on the "NoXP" menu in the main menu of Unity and select "Shelves" menu item |
| The "Shelves" window will pop up where you can load and manage your current active shelf configuration |
| Click on "Open" navigate to the previously created folder, find the "Data" subfolder and select the ShelvesCFG.scfg file to load the default shelves (providing the core Unity objects and components). | ![Project View](/Documentation/Doc_Shelves.jpg) |
| Afterwards click on the little "eye" icon subscripted with "all" next to the "ShelvesCFG.scfg" entry in the "Shelves" window, this will display all shelf windows configured in the selected shelves config. |

![Project View](/Documentation/Doc_ShelfWindow.jpg) 

Most other elements are described via tooltips, just take a look around and try it out

### Contribution guidelines ###

* Feel free to make your own fork of this repository or suggest features or fixes (via pull request or message)
* If you find this utility helpful and use it on your every days work, send me a note or mail, I'm always interested in what other people do and achieve and where my ideas might have helped.

### Distribution ###
* The code or parts of it is not meant to be redistributed in a commercial manner, it is a utility which should help anyone to simplify their work without any demands.

### Who do I talk to? ###

* Tobias (that's me, for more information about me and my projects go to http://www.tobiaspott.de)