﻿using NoXP.Shelves;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace NoXP.Shelves
{

    public class ShelfItem
    {
        public enum Types
        {
            Button,
            Header,
            Separator
        }

        public virtual void Draw(ref ShelfItemResponse response, ShelfGUIOrientation orientation, ShelfGUISize size)
        { }

        public virtual void ExecuteCommands()
        { }

        public virtual float GetSize(ShelfGUISize size)
        { return 0.0f; }
    }

    public class ShelfSeparator : ShelfItem
    {
        private static ShelfSeparator _instance = null;
        public static ShelfSeparator Instance
        {
            get
            {
                if (_instance == null) _instance = new ShelfSeparator();
                return _instance;
            }
        }

        public override void Draw(ref ShelfItemResponse response, ShelfGUIOrientation orientation, ShelfGUISize size)
        {
            GUILayout.Space(ShelfStyles.ShelfSeparatorSize);
        }


        private ShelfSeparator()
        { }

        public override float GetSize(ShelfGUISize size)
        { return ShelfStyles.ShelfSeparatorSize; }

    }

    public class ShelfHeader : ShelfItem
    {
        private string _text = string.Empty;
        // properties
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        // ctors
        public ShelfHeader(string text = "")
        {
            _text = text;
        }

        public override void Draw(ref ShelfItemResponse response, ShelfGUIOrientation orientation, ShelfGUISize size)
        {
            if (!_text.Equals(string.Empty))
            {
                Rect lastRect;
                if (orientation == ShelfGUIOrientation.Horizontal)
                {
                    GUILayout.Space(1.0f);
                    lastRect = GUILayoutUtility.GetLastRect();
                    GUILayout.Space(ShelfStyles.ShelfHeaderSize - 1.0f);
                    Matrix4x4 translate = Matrix4x4.TRS(new Vector3(-ShelfStyles.ShelfButtonSize[ShelfGUISize.Normal] - 2, 0, 0), Quaternion.identity, Vector3.one);

                    float angle = -90.0f;
                    GUIUtility.RotateAroundPivot(angle, new Vector2(lastRect.x, 0)); //  ShelfStyles.ShelfButtonHeight[ShelfGUISize.Normal]));
                                                                                     // apply translation matrix
                    GUI.matrix *= translate;
                    lastRect.y = 0.0f; // clear y coordinate
                    lastRect.width = ShelfStyles.ShelfButtonSize[ShelfGUISize.Normal];
                    lastRect.height = ShelfStyles.ShelfHeaderSize;

                }
                else
                {
                    GUILayout.Label(" ", ShelfStyles.ShelfItemHeaderStyle, GUILayout.MaxHeight(ShelfStyles.ShelfHeaderSize), GUILayout.MaxWidth(ShelfStyles.ShelfButtonSize[ShelfGUISize.Normal]));
                    lastRect = GUILayoutUtility.GetLastRect();
                    lastRect.x -= 4.0f;
                    lastRect.width = ShelfStyles.ShelfButtonSize[ShelfGUISize.Normal];
                }

                // draw background color rect
                EditorGUI.DrawRect(lastRect, ShelfStyles.ShelfItemHeaderBackground);
                GUI.Label(lastRect, _text, ShelfStyles.ShelfItemHeaderStyle);
                // revert to default rotation matrix
                GUI.matrix = Matrix4x4.identity;
            }

        }

        public override float GetSize(ShelfGUISize size)
        { return ShelfStyles.ShelfHeaderSize; }

        public override string ToString()
        {
            return base.ToString() + string.Format(";Text:{0}", this.Text);
        }
    }

    public class ShelfButton : ShelfItem
    {
        public static readonly Type Type = typeof(ShelfButton);

        private string _name = string.Empty;
        private string _tooltip = string.Empty;
        private string _iconPath = string.Empty;

        private float _guiSize = 0.0f;
        private GUIContent _guiContent = new GUIContent();
        private Texture2D _icon = null;
        private List<Command> _commands = new List<Command>();

        // properties
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                this.RefreshGUIContent();
            }
        }
        public string Tooltip
        {
            get { return _tooltip; }
            set { _tooltip = value; }
        }
        public string IconPath
        {
            get { return _iconPath; }
            set { this.SetIconPath(value); }
        }
        public Texture2D Icon
        { get { return _icon; } }
        public GUIContent GUIContent
        { get { return _guiContent; } }

        // ctors
        public ShelfButton(string name, string tooltip, string iconPath)
        {
            _name = name;
            _tooltip = tooltip;
            this.SetIconPath(iconPath);
        }

        // methods
        private void SetIconPath(string iconPath)
        {
            _iconPath = iconPath;
            if (!_iconPath.Equals(string.Empty))
                _icon = (Texture2D)EditorGUIUtility.Load(iconPath);
            this.RefreshGUIContent();
        }

        private void RefreshGUIContent()
        {
            _guiContent.tooltip = _tooltip;
            if (_icon != null)
            {
                _guiContent.image = _icon;
                _guiContent.text = _name;
            }
            else
            {
                _guiContent.image = null;
                _guiContent.text = _name;
            }
        }

        public override void Draw(ref ShelfItemResponse response, ShelfGUIOrientation orientation, ShelfGUISize size)
        {
            // update required width
            float guiWidth = ShelfStyles.ShelfButtonSize[size];
            float guiHeight = ShelfStyles.ShelfButtonSize[size];
            
            // ! ! ! ! 
            // -> determine if the label text requires more space than one line and increase the labels size accordingly
            //   -> 


            bool result = false;
            result = GUILayout.Button(_guiContent, ShelfStyles.ShelfItemButtonStyle, GUILayout.MaxHeight(guiHeight), GUILayout.MaxWidth(guiWidth));

            Rect lastRect = GUILayoutUtility.GetLastRect();
            //GUI.Label(lastRect, _guiContent, ShelfStyles.ShelfItemButtonTextStyle);

            // ! ! ! !
            // draw the text overlay (make this one an option to display or hide)
            Vector2 btnSize = ShelfStyles.ShelfItemButtonTextStyle.CalcSize(_guiContent);
            lastRect.y = lastRect.yMax - btnSize.y;
            lastRect.height = btnSize.y;
            lastRect.x += 2;
            lastRect.width -= 2;
            if (EditorGUIUtility.isProSkin)
                EditorGUI.DrawRect(lastRect, ShelfStyles.ShelfItemButtonTextBackgroundPro);
            else
                EditorGUI.DrawRect(lastRect, ShelfStyles.ShelfItemButtonTextBackgroundLite);

            GUI.Label(lastRect, _guiContent, ShelfStyles.ShelfItemButtonTextStyle);

            if (result && !response.Triggered)
            {
                response.Triggered = true;
                response.Item = this;
                response.SetData(Event.current);
            }
        }

        public override void ExecuteCommands()
        {
            for (int i = 0; i < _commands.Count; i++)
                _commands[i].Execute();
        }

        public void Add(Command item)
        {
            if (!_commands.Contains(item))
                _commands.Add(item);
        }
        public void Remove(Command item)
        {
            if (_commands.Contains(item))
                _commands.Add(item);
        }
        public void Clear()
        {
            _commands.Clear();
        }

        public override float GetSize(ShelfGUISize size)
        {
            return ShelfStyles.ShelfButtonSize[size];
        }

        public override string ToString()
        {
            return base.ToString() + string.Format(";Name:{0}", this.Name);
        }
    }

    public class ShelfGroup : ShelfItem
    {
        public static readonly Type Type = typeof(ShelfGroup);

        private bool _collapsed = false;
        private List<ShelfItem> _items = new List<ShelfItem>();

        private GUIContent _guiContent = new GUIContent();

        public List<ShelfItem> Items
        { get { return _items; } }

        public string Name
        {
            get { return _guiContent.text; }
            set { _guiContent.text = value; }
        }
        // ctors
        public ShelfGroup(string text, string tooltip = "")
        {
            _guiContent.text = text;
            _guiContent.tooltip = tooltip;
        }

        // methods
        public void Add(ShelfItem item)
        { _items.Add(item); }
        public void AddRange(IEnumerable<ShelfItem> items)
        { _items.AddRange(items); }
        public void Remove(ShelfItem item)
        {
            while (_items.Contains(item))
            {
                _items.Add(item);
            };
        }
        public void Clear()
        { _items.Clear(); }

        public override void Draw(ref ShelfItemResponse response, ShelfGUIOrientation orientation, ShelfGUISize size)
        {
            bool result = false;
            Rect lastRect;

            // prepare drawing destinations and ui transformation matrix
            if (orientation == ShelfGUIOrientation.Horizontal)
            {
                GUILayout.Space(1.0f);
                lastRect = GUILayoutUtility.GetLastRect();
                GUILayout.Space(ShelfStyles.ShelfGroupSize - 1.0f);
                Matrix4x4 translate = Matrix4x4.TRS(new Vector3(-ShelfStyles.ShelfButtonSize[ShelfGUISize.Normal] - 2, 0, 0), Quaternion.identity, Vector3.one);

                float angle = -90.0f;
                GUIUtility.RotateAroundPivot(angle, new Vector2(lastRect.x, 0)); //  ShelfStyles.ShelfButtonHeight[ShelfGUISize.Normal]));
                                                                                 // apply translation matrix
                GUI.matrix *= translate;
                // draw background color rect
                lastRect.y = 0.0f; // clear y coordinate
                lastRect.width = ShelfStyles.ShelfButtonSize[ShelfGUISize.Normal];
                lastRect.height = ShelfStyles.ShelfGroupSize;
            }
            else
            {
                GUILayout.Label(" ", ShelfStyles.ShelfItemHeaderStyle, GUILayout.MaxHeight(ShelfStyles.ShelfGroupSize), GUILayout.MaxWidth(ShelfStyles.ShelfButtonSize[ShelfGUISize.Normal]));
                lastRect = GUILayoutUtility.GetLastRect();
                lastRect.x -= 4.0f;
                lastRect.width = ShelfStyles.ShelfButtonSize[ShelfGUISize.Normal];
            }

            // draw actual button and process for event handling
            result = GUI.Button(lastRect, _guiContent, ShelfStyles.ShelfItemGroupStyle);
            if (result && !response.Triggered)
            {
                response.Triggered = true;
                response.Item = this;
                response.SetData(Event.current);
            }
            // toggle collapse state only when clicked with left mouse button
            if (result && response.Triggered
                && response.MouseButton == 0
                /*&& response.ClickCount == 1*/)
                _collapsed = !_collapsed;

            GUI.matrix = Matrix4x4.identity;

            // draw the group's items (just like the normal shelf items are drawn)
            if (!_collapsed)
            {
                if (orientation == ShelfGUIOrientation.Horizontal)
                {
                    using (new EditorGUILayout.HorizontalScope(GUILayout.MaxWidth(this.GetSize(size))))
                        ShelfUtility.DrawItems(_items, ref response, orientation, size);
                }
                else
                {
                    using (new EditorGUILayout.VerticalScope(GUILayout.MaxHeight(this.GetSize(size))))
                        ShelfUtility.DrawItems(_items, ref response, orientation, size);
                }
            }
        }

        public override float GetSize(ShelfGUISize size)
        {
            float result = 0.0f;
            if (size == ShelfGUISize.Small)
            {
                int btnCnt = 0;
                for (int i = 0; i < _items.Count; i++)
                {
                    Type t = _items[i].GetType();

                    if (t.Equals(typeof(ShelfButton)))
                    {
                        if (btnCnt == 1
                            || i == _items.Count - 1)
                        {
                            btnCnt = 0;
                            result += _items[i].GetSize(size);
                        }
                        else
                            btnCnt++;
                    }
                    else
                    {
                        // reset button count and add size of button + whatever comes next
                        if (btnCnt != 0)
                            result += ShelfStyles.ShelfButtonSize[ShelfGUISize.Small];
                        result += _items[i].GetSize(size);
                        btnCnt = 0;
                    }
                }
            }
            else
            {
                for (int i = 0; i < _items.Count; i++)
                    result += _items[i].GetSize(size);
            }
            return result;
        }

        public override string ToString()
        {
            return base.ToString() + string.Format(";Name:{0}", this.Name);
        }
    }

    public class Shelf : ShelfItem
    {
        private string _name = string.Empty;
        private string _tooltip = string.Empty;

        private float _guiWidth = 0.0f;
        private GUIContent _guiContent = new GUIContent();
        private Vector2 _scrollPosition = Vector2.zero;
        private List<ShelfItem> _items = new List<ShelfItem>();
        private string _sourceFile = string.Empty;

        public List<ShelfItem> Items
        { get { return _items; } }

        private float GUIWidth
        {
            get
            {
                _guiWidth = Mathf.Max(24.0f, ShelfStyles.ShelfStyleNormal.CalcSize(_guiContent).x);
                return _guiWidth;
            }
        }
        // properties
        public string Name
        {
            get { return _name; }
            set
            {
                _guiContent.text = _name = value;
                //_guiWidth = Mathf.Max(24.0f, ShelfStyles.ShelfStyleNormal.CalcSize(_guiContent).x);
            }
        }
        public string Tooltip
        {
            get { return _tooltip; }
            set { _guiContent.tooltip = _tooltip = value; }
        }
        public GUIContent GUIContent
        { get { return _guiContent; } }
        public string SourceFile
        {
            get { return _sourceFile; }
            set { _sourceFile = value; }
        }

        // ctors
        public Shelf(string name, string tooltip)
        {
            this.Name = name;
            this.Tooltip = tooltip;
        }

        // methods
        public void Add(ShelfItem item)
        {
            // if (!_items.Contains(item))
            _items.Add(item);
        }
        public void AddRange(IEnumerable<ShelfItem> items)
        {
            // if (!_items.Contains(item))
            _items.AddRange(items);
        }
        public void Remove(ShelfItem item)
        {
            while (_items.Contains(item))
            {
                _items.Add(item);
            };
        }
        public void Clear()
        {
            _items.Clear();
        }

        public bool DrawHorizontal(GUIStyle style = null)
        {
            return GUILayout.Button(_guiContent, style, GUILayout.MaxWidth(this.GUIWidth));
        }
        public bool DrawVertical(ref Vector2 offset, Matrix4x4 uiTransform, GUIStyle style = null)
        {
            return ShelfWindow.DrawVerticalButton(_guiContent, ref offset, uiTransform, style);
        }

        public bool Draw(ShelfGUIOrientation orientation, ShelfGUISize size)
        {
            if (orientation == ShelfGUIOrientation.Horizontal)
            {
                _guiContent.text = _name;
                return GUILayout.Button(_guiContent, ShelfStyles.ShelfStyleNormal, GUILayout.MaxWidth(this.GUIWidth));
            }
            else
            {
                if (this.GUIWidth > Screen.width - 12.0f)
                {
                    if (_guiContent.text.Length > 7) // ! ! ! ! might need adjustment when over all sizes are increased
                        _guiContent.text = _name.Substring(0, 7) + "..";
                }
                else
                    _guiContent.text = _name;
                return GUILayout.Button(_guiContent, ShelfStyles.ShelfStyleNormal);
            }
        }

        public void DrawItems(ref ShelfItemResponse response, ShelfGUIOrientation orientation, ShelfGUISize size)
        {
            if (orientation == ShelfGUIOrientation.Horizontal)
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    _scrollPosition.y = 1.0f;
                    _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition, true, false);
                    _scrollPosition.y = 1.0f;

                    ShelfUtility.DrawItems(_items, ref response, orientation, size);

                    EditorGUILayout.EndScrollView();
                }
            }
            else
            {
                using (new EditorGUILayout.VerticalScope())
                {
                    _scrollPosition.x = 0.0f;
                    _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition, false, true);
                    _scrollPosition.x = 0.0f;

                    ShelfUtility.DrawItems(_items, ref response, orientation, size);

                    EditorGUILayout.EndScrollView();
                }
            }

        }

    }

}