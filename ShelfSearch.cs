﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

namespace NoXP.Shelves
{
    public class ShelfSearch : EditorWindow
    {
        private const string ControlName_TextField = "NoXP.Shelves.Search.TextField";

        // singleton
        private static ShelfSearch _instance = null;
        public ShelfSearch Instance
        {
            get
            {
                if (_instance == null)
                    ShelfSearch.DisplayAsDropDown(new Rect(Input.mousePosition, new Vector2(1, 1)), new Vector2(84, 200));
                return _instance;
            }
        }


        private Shelf _searchShelf = null;
        private List<ShelfItem> _searchTargets = new List<ShelfItem>();
        private List<ShelfItem> _searchResults = new List<ShelfItem>();


        public static ShelfSearch DisplayAsDropDown(Rect sourceRect, Vector2 size)
        {
            if (_instance == null)
                _instance = EditorWindow.CreateInstance<ShelfSearch>();
            _instance.ShowAsDropDown(sourceRect, size);
            return _instance;
        }


        private Vector2 _scrollPosition = Vector2.zero;
        private float _borderThickness = 2.0f;

        static Color32 clrPro = new Color32(56, 56, 56, 255);
        static Color32 clrLite = new Color32(194, 194, 194, 255);


        public void SetSearchTarget(Shelf searchTarget)
        {
            _searchResults.Clear();
            _searchShelf = searchTarget;
            _searchResults.AddRange(_searchShelf.Items);
        }
        public void SetSearchTargets(params Shelf[] shelves)
        {
            _searchResults.Clear();
            foreach (Shelf shelf in shelves)
                _searchTargets.AddRange(shelf.Items);
            // either add a "clone" function to work on unreferenced copies
            // -> or add an additional draw function to the ShelfItem class which is used for SearchResult display only
        }

        private void OnGUI()
        {
            Rect innerRect = new Rect(_borderThickness, _borderThickness, Screen.width - _borderThickness * 2, Screen.height - _borderThickness * 2);
            EditorGUI.DrawRect(new Rect(0, 0, Screen.width, Screen.height), EditorGUIUtility.isProSkin ? clrLite : clrPro); // new Color(1.0f, 0.5f, 0.0f, 1.0f));
            EditorGUI.DrawRect(innerRect, EditorGUIUtility.isProSkin ? clrPro : clrLite); // new Color32(56, 56, 56, 255));

            ShelfItemResponse response = ShelfItemResponse.Current;
            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Space(_borderThickness);
                using (new EditorGUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.MinHeight(this.position.height)))
                {
                    GUILayout.Space(_borderThickness);
                    GUILayout.Label("Search:", EditorStyles.miniLabel);
                    this.GUISearch();
                    GUILayout.Space(-_borderThickness * 2);

                    using (EditorGUILayout.ScrollViewScope scrollView = new EditorGUILayout.ScrollViewScope(_scrollPosition, false, true))
                    {
                        _scrollPosition = scrollView.scrollPosition;

                        using (new EditorGUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.MinHeight(this.position.height)))
                        {
                            if (_searchShelf != null)
                                foreach (ShelfItem item in _searchResults)
                                {
                                    item.Draw(ref response, ShelfGUIOrientation.Vertical, ShelfGUISize.Small);
                                }

                        }

                    }
                    GUILayout.Space(_borderThickness);
                }
                GUILayout.Space(_borderThickness);
            }
            // execute the commands of the clicked ShelfItem
            if (response.Triggered
                && response.MouseButton == 0)
                response.Item.ExecuteCommands();
        }


        string _searchValue = string.Empty;
        bool _hasFocus = false;

        private void GUISearch()
        {
            // move the keyDown and keyCode==Enter/Return check before the textfield as it would otherwise eat the event for text input
            if (Event.current.type == EventType.KeyDown)
            {
                if (Event.current.keyCode == KeyCode.Return)
                {
                    if (!_searchValue.Equals(string.Empty))
                        ShelfWindow.FilterShelves(_searchShelf, _searchValue, _searchResults);
                    Debug.Log("Execute first item of result list!");
                    this.Close();
                    _searchValue = string.Empty;
                }
                else if (Event.current.keyCode == KeyCode.Escape)
                {
                    this.Close();
                }
            }
            EditorGUI.BeginChangeCheck();

            GUI.SetNextControlName(ShelfSearch.ControlName_TextField);
            _searchValue = EditorGUILayout.TextField(_searchValue, ShelfStyles.ShelfSearchStyle, GUILayout.MaxWidth(96.0f), GUILayout.MinHeight(ShelfStyles.ShelfMenuSize));

            // if any change occured update the search filter and the results
            if (EditorGUI.EndChangeCheck())
                ShelfWindow.FilterShelves(_searchShelf, _searchValue, _searchResults);

            // set focus on textfield on first draw (only works after control has received its name)
            if (!_hasFocus)
            {
                EditorGUI.FocusTextInControl(ShelfSearch.ControlName_TextField);
                _hasFocus = true;
            }
        }

    }

}