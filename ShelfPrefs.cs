﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;

namespace NoXP.Shelves
{

    [InitializeOnLoad]
    public class ShelfPrefs : EditorWindow
    {
        public const string EDPREF_SHELFCFG = "NoXP.Shelves.CFG";
        public const string EDPREF_SHELFCFGHISTORY = "NoXP.Shelves.CFG.History";



        private static List<ShelfWindow> _allWindows = new List<ShelfWindow>();
        private static List<string> _recentHistory = new List<string>(); // just limit to 20 as it seems to be far more then required (other software uses 5 to 10 recent slots!)

        // store the _cfg path into a EditorPref value to auto-restore it on follow-up unity sessions
        // ! ! ! ! 
        // move _xmlConfig and ..Path (and perhabs the _instances list) to the ShelfPrefs class to undock it from the ShelfWindow class
        // ! ! ! ! 
        private static XDocument _xmlConfig = null;
        private static string _xmlConfigPath = string.Empty;

        private static int _recentPreviewIndex = -1;
        private static XDocument _recentPreviewDocument = null;
        private static IEnumerable<XElement> _recentPreviewElements = null;

        public static XDocument XmlConfig
        { get { return _xmlConfig; } }


        public static void UpdateConfigPath()
        {
            if (_xmlConfigPath.Equals(string.Empty))
                _xmlConfigPath = EditorPrefs.GetString(ShelfPrefs.EDPREF_SHELFCFG, _xmlConfigPath);
        }
        public static void SaveConfig()
        {
            if (_xmlConfig != null && !_xmlConfigPath.Equals(string.Empty))
                _xmlConfig.Save(_xmlConfigPath);
        }
        public static void ReloadConfig()
        {
            if (File.Exists(_xmlConfigPath))
                _xmlConfig = XDocument.Load(_xmlConfigPath);
        }
        public static bool LoadConfig(string configPath)
        {
            if (File.Exists(configPath))
            {
                _xmlConfig = XDocument.Load(configPath);
                _xmlConfigPath = configPath;
                EditorPrefs.SetString(ShelfPrefs.EDPREF_SHELFCFG, _xmlConfigPath);

                // append loaded cfg to the recent history
                ShelfPrefs.AppendRecentHistory(_xmlConfigPath);
                return true;
            }
            return false;
        }


        private Vector2 _scrollPosition = Vector2.zero;


        [MenuItem("NoXP/Shelves", false, 0)]
        public static void ShowPreferences()
        {
            // DEBUG: clear editor pref keys
            ShelfPrefs window = EditorWindow.GetWindow<ShelfPrefs>(true, "Shelves");
            window.minSize = new Vector2(200, 300);
            window.maxSize = new Vector2(202, 302);
            window.autoRepaintOnSceneChange = true;
            window.Focus();
            ShelfPrefs.Refresh();


            Texture2D tex = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Editor/Shelves/UI/UIRES_Cancel.png");
            Debug.Log("Has loaded: " + tex);
        }

        static ShelfPrefs()
        {
            EditorApplication.delayCall += new EditorApplication.CallbackFunction(Init);
        }



        private static bool _shelvesLoaded = false;

        public static void Init()
        {
            if (!_shelvesLoaded)
            {
                Debug.Log("Reload all shelves from last recent config");
                ShelfPrefs.UpdateConfigPath();
                ShelfPrefs.ReloadConfig();
                ShelfWindow.LoadShelf(true);
                _shelvesLoaded = true;
            }
        }

        public static void Refresh()
        {
            _allWindows.Clear();
            _allWindows.AddRange(Resources.FindObjectsOfTypeAll<ShelfWindow>());
            // read config history from editor prefs
            string prefRecentHistory = EditorPrefs.GetString(ShelfPrefs.EDPREF_SHELFCFGHISTORY, string.Empty);
            if (!prefRecentHistory.Equals(string.Empty))
            {
                _recentHistory.Clear();
                _recentHistory.AddRange(prefRecentHistory.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
            }

            int prevPreviewIndex = _recentPreviewIndex;
            ShelfPrefs.ClearPreview();
            ShelfPrefs.Preview(prevPreviewIndex);
            // focus ShelfPrefs window to regain focus and update painting
            EditorWindow.FocusWindowIfItsOpen<ShelfPrefs>();
        }


        private void OnGUI()
        {

            using (new EditorGUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(false)))
            {
                using (EditorGUILayout.ScrollViewScope scrollview = new EditorGUILayout.ScrollViewScope(_scrollPosition))
                {
                    _scrollPosition = scrollview.scrollPosition;

                    using (new EditorGUILayout.HorizontalScope())
                    {
                        GUILayout.Label("Configurations", ShelfStyles.ShelfPrefsNormalLeftBoldStyle, GUILayout.ExpandWidth(true));
                    }

                    using (new EditorGUILayout.HorizontalScope())
                    {
                        if (GUILayout.Button("Open", ShelfStyles.ShelfPrefsNormalLeftStyle, GUILayout.ExpandWidth(false)))
                        {
                            ShelfPrefs.ClearPreview();
                            ShelfWindow.LoadShelfFromOpenDialog();
                            ShelfPrefs.Refresh();
                        }
                        if (GUILayout.Button(ShelfResources.UIRES_Refresh, ShelfStyles.ShelfPrefsNormalLeftStyle, GUILayout.ExpandWidth(false)))
                            ShelfPrefs.Refresh();

                        GUILayout.Label("", ShelfStyles.ShelfPrefsNormalLeftBoldStyle, GUILayout.ExpandWidth(true));
                    }
                    EditorGUILayout.Separator();

                    Uri projPath = new Uri(Application.dataPath, UriKind.Absolute);
                    // list cfg files of recent sessions/selections
                    for (int i = 0; i < _recentHistory.Count; i++)
                    {
                        // check if the current one is the active chelf config
                        bool isActive = _recentHistory[i].Equals(_xmlConfigPath);
                        Uri dirPath = new Uri(Path.GetDirectoryName(_recentHistory[i]), UriKind.RelativeOrAbsolute);
                        string fileName = Path.GetFileName(_recentHistory[i]);
                        if (fileName.Length > 20)
                            fileName = ".." + fileName.Substring(fileName.Length - 20, 20);

                        using (new EditorGUILayout.HorizontalScope())
                        {
                            if (GUILayout.Button(isActive ? ShelfResources.UIRES_CheckBox_Checked : ShelfResources.UIRES_CheckBox_Unchecked, ShelfStyles.ShelfPrefsIconStyle,
                                GUILayout.MaxWidth(20.0f), GUILayout.ExpandWidth(false)))
                            {

                                if (!isActive)
                                {
                                    ShelfPrefs.ClearPreview();
                                    ShelfPrefs.LoadConfig(_recentHistory[i]);
                                    ShelfWindow.LoadShelfFromXDocument(ShelfPrefs.XmlConfig);
                                }
                                else
                                {
                                    ShelfPrefs.ReloadConfig();
                                    ShelfWindow.LoadShelf();
                                }

                                ShelfPrefs.Refresh();
                                this.Focus();
                            }

                            if (GUILayout.Button(new GUIContent(fileName, projPath.MakeRelativeUri(dirPath).ToString() + Path.AltDirectorySeparatorChar),
                                isActive ? ShelfStyles.ShelfPrefsNormalLeftBoldStyle : ShelfStyles.ShelfPrefsNormalLeftStyle, GUILayout.ExpandWidth(true)))
                            {
                                ShelfPrefs.Preview(i);
                            }
                            if (isActive)
                            {
                                if (GUILayout.Button(ShelfResources.UIRES_VisibleAll, EditorStyles.toolbarButton, GUILayout.MaxWidth(32.0f), GUILayout.ExpandWidth(false)))
                                {
                                    for (int w = _allWindows.Count - 1; w >= 0; w--)
                                        _allWindows[w].Close();

                                    ShelfPrefs.ShowAllWindows(_recentHistory[i]);
                                    ShelfPrefs.ReloadConfig();
                                    ShelfWindow.LoadShelf();
                                    ShelfPrefs.Refresh();
                                }
                            }
                            if (GUILayout.Button("X", EditorStyles.toolbarButton, GUILayout.ExpandWidth(false)))
                            {
                                Debug.Log("Remove " + _recentHistory[i]);
                                ShelfPrefs.RemoveRecentHistory(_recentHistory[i]);
                            }
                        }


                        if (i == _recentPreviewIndex
                            && i < _recentHistory.Count
                            && _recentPreviewDocument != null
                            && _recentPreviewElements != null)
                        {

                            if (_recentHistory[i].Equals(_xmlConfigPath))
                            {
                                for (int w = _allWindows.Count - 1; w >= 0; w--)
                                // for (int w = 0; w < _allWindows.Count; w++)
                                {
                                    using (new EditorGUILayout.HorizontalScope())
                                    {
                                        XElement elWindow = _allWindows[w].ConfigNode;

                                        XAttribute attrTitle = elWindow.Attribute("title");
                                        string vTitle = attrTitle != null ? attrTitle.Value : string.Empty;
                                        XAttribute attrVisible = elWindow.Attribute("visible");
                                        bool vVisible = attrVisible != null ? bool.Parse(attrVisible.Value) : true;

                                        GUILayout.Label("", ShelfStyles.ShelfPrefsNormalLeftStyle, GUILayout.MaxWidth(8.0f), GUILayout.ExpandWidth(false));
                                        GUILayout.Label(vTitle, ShelfStyles.ShelfPrefsNormalLeftStyle, GUILayout.ExpandWidth(true));
                                        if (GUILayout.Button(vVisible ? ShelfResources.UIRES_VisibleOn : ShelfResources.UIRES_VisibleOff, ShelfStyles.ShelfPrefsNormalLeftStyle, GUILayout.MaxWidth(32.0f)))
                                        {
                                            //Debug.Log(vTitle + ":" + vVisible);
                                            vVisible = !vVisible;
                                            attrVisible.Value = vVisible.ToString().ToLowerInvariant();
                                            _allWindows[w].SaveWindowCFG();

                                            ShelfPrefs.SaveConfig();
                                            ShelfWindow.LoadShelf();
                                            ShelfPrefs.Refresh();
                                        }

                                    }

                                }
                            }
                            else
                            {
                                foreach (XElement elWindow in _recentPreviewElements)
                                {
                                    using (new EditorGUILayout.HorizontalScope())
                                    {
                                        XAttribute attrTitle = elWindow.Attribute("title");
                                        string vTitle = attrTitle != null ? attrTitle.Value : string.Empty;
                                        XAttribute attrVisible = elWindow.Attribute("visible");
                                        bool vVisible = attrVisible != null ? bool.Parse(attrVisible.Value) : true;

                                        GUILayout.Label("", ShelfStyles.ShelfPrefsNormalLeftStyle, GUILayout.MaxWidth(8.0f), GUILayout.ExpandWidth(false));
                                        GUILayout.Label(vTitle, ShelfStyles.ShelfPrefsNormalLeftStyle, GUILayout.ExpandWidth(true));
                                        if (GUILayout.Button(vVisible ? ShelfResources.UIRES_VisibleOn : ShelfResources.UIRES_VisibleOff, ShelfStyles.ShelfPrefsNormalLeftStyle, GUILayout.MaxWidth(32.0f)))
                                        {
                                            vVisible = !vVisible;
                                            attrVisible.Value = vVisible.ToString().ToLowerInvariant();
                                            _recentPreviewDocument.Save(_recentHistory[_recentPreviewIndex]);
                                        }

                                    }

                                }

                            }

                            // draw separator under current active 
                            if (isActive)
                            {
                                using (new EditorGUILayout.HorizontalScope())
                                    EditorGUILayout.Separator();
                            }

                        }


                    }

                }

            }

        }

        private static void ShowAllWindows(string configPath)
        {
            if (File.Exists(configPath))
            {
                XNamespace xNs = "http://tempuri.org/ShelvesCFG.xsd";
                XDocument xDoc = XDocument.Load(configPath);
                IEnumerable<XElement> elsWindow = xDoc.Root.Elements(xNs + "Window");
                foreach (XElement elWindow in elsWindow)
                    elWindow.Attribute("visible").SetValue("true");
                xDoc.Save(configPath);
            }
        }

        private static void ClearPreview()
        {
            _recentPreviewIndex = -1;
            _recentPreviewDocument = null;
            _recentPreviewElements = null;
        }

        private static void Preview(int index)
        {
            if (index == -1
                || _recentPreviewIndex == index)
            {
                ShelfPrefs.ClearPreview();
                return;
            }

            if (_recentPreviewIndex != index)
            {
                _allWindows.Clear();
                _allWindows.AddRange(Resources.FindObjectsOfTypeAll<ShelfWindow>());

                _recentPreviewIndex = index;
                if (index < _recentHistory.Count
                    && File.Exists(_recentHistory[_recentPreviewIndex]))
                {
                    XNamespace xNs = "http://tempuri.org/ShelvesCFG.xsd";
                    _recentPreviewDocument = XDocument.Load(_recentHistory[_recentPreviewIndex]);
                    _recentPreviewElements = _recentPreviewDocument.Root.Elements(xNs + "Window");
                }

            }
        }


        public static void AppendRecentHistory(string recent)
        {
            if (!_recentHistory.Contains(recent))
            {
                if (_recentHistory.Count == 0)
                    _recentHistory.Add(recent);
                else
                    _recentHistory.Insert(0, recent);
            }
            else
            {
                _recentHistory.Remove(recent);
                _recentHistory.Insert(0, recent);
            }
            EditorPrefs.SetString(ShelfPrefs.EDPREF_SHELFCFGHISTORY, string.Join(";", _recentHistory.ToArray()));
        }
        public static void RemoveRecentHistory(string recent)
        {
            if (_recentHistory.Contains(recent))
                _recentHistory.Remove(recent);
            EditorPrefs.SetString(ShelfPrefs.EDPREF_SHELFCFGHISTORY, string.Join(";", _recentHistory.ToArray()));
        }
        public static void ClearRecentHistory()
        {
            _recentHistory.Clear();
        }

    }

}