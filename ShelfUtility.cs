﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;

namespace NoXP.Shelves
{

    public class ShelfStyles
    {
        public static readonly Color SkinColorDark = new Color32(56, 56, 56, 255);
        public static readonly Color SkinColorLight = new Color32(194, 194, 194, 255);

        private static GUIStyle _shelfPrefsNormalLeftStyle = null;
        public static GUIStyle ShelfPrefsNormalLeftStyle
        {
            get
            {
                if (_shelfPrefsNormalLeftStyle == null)
                {
                    _shelfPrefsNormalLeftStyle = new GUIStyle(EditorStyles.toolbarButton);
                    _shelfPrefsNormalLeftStyle.alignment = TextAnchor.MiddleLeft;
                }
                return _shelfPrefsNormalLeftStyle;
            }
        }

        private static GUIStyle _shelfPrefsNormalLeftBoldStyle = null;
        public static GUIStyle ShelfPrefsNormalLeftBoldStyle
        {
            get
            {
                if (_shelfPrefsNormalLeftBoldStyle == null)
                {
                    _shelfPrefsNormalLeftBoldStyle = new GUIStyle(EditorStyles.toolbarButton);
                    _shelfPrefsNormalLeftBoldStyle.alignment = TextAnchor.MiddleLeft;
                    _shelfPrefsNormalLeftBoldStyle.fontStyle = FontStyle.Bold;
                }
                return _shelfPrefsNormalLeftBoldStyle;
            }
        }

        private static GUIStyle _shelfPrefsIconStyle = null;
        public static GUIStyle ShelfPrefsIconStyle
        {
            get
            {
                if (_shelfPrefsIconStyle == null)
                {
                    _shelfPrefsIconStyle = new GUIStyle(EditorStyles.toolbarButton);
                    _shelfPrefsIconStyle.alignment = TextAnchor.MiddleCenter;
                    _shelfPrefsIconStyle.stretchWidth = false;
                    _shelfPrefsIconStyle.padding = new RectOffset(0, 0, 1, 1);
                    _shelfPrefsIconStyle.stretchWidth = false;
                }
                return _shelfPrefsIconStyle;
            }
        }


        private static GUIStyle _shelfStyleNormal = null;
        public static GUIStyle ShelfStyleNormal
        {
            get
            {
                if (_shelfStyleNormal == null)
                {
                    _shelfStyleNormal = new GUIStyle(EditorStyles.toolbarButton);
                    _shelfStyleNormal.alignment = TextAnchor.MiddleLeft;
                    _shelfStyleNormal.fixedHeight = ShelfStyles.ShelfMenuSize;
                    _shelfStyleNormal.fontSize = 11;
                }
                return _shelfStyleNormal;
            }
        }

        private static GUIStyle _shelfStyleSelected = null;
        public static GUIStyle ShelfStyleSelected
        {
            get
            {
                if (_shelfStyleSelected == null)
                {
                    _shelfStyleSelected = new GUIStyle(EditorStyles.toolbarButton);
                    _shelfStyleSelected.alignment = TextAnchor.MiddleLeft;
                    _shelfStyleSelected.normal = ShelfStyleSelected.onNormal;
                    _shelfStyleSelected.fixedHeight = ShelfStyles.ShelfMenuSize;
                    _shelfStyleSelected.fontSize = 11;
                }
                return _shelfStyleSelected;
            }
        }

        private static GUIStyle _shelfIconStyle = null;
        public static GUIStyle ShelfIconStyle
        {
            get
            {
                if (_shelfIconStyle == null)
                {
                    _shelfIconStyle = new GUIStyle(EditorStyles.toolbarButton);
                    _shelfIconStyle.alignment = TextAnchor.MiddleCenter;
                    _shelfIconStyle.padding = new RectOffset(2, 1, 2, 1);
                    _shelfIconStyle.imagePosition = ImagePosition.ImageOnly;
                    _shelfIconStyle.fixedHeight = ShelfStyles.ShelfMenuSize;
                }
                return _shelfIconStyle;
            }
        }

        private static GUIStyle _shelfSearchStyle = null;
        public static GUIStyle ShelfSearchStyle
        {
            get
            {
                if (_shelfSearchStyle == null)
                {
                    _shelfSearchStyle = new GUIStyle(EditorStyles.toolbarTextField);
                    _shelfSearchStyle.fixedHeight = ShelfMenuSize - 4.0f;
                    _shelfSearchStyle.alignment = TextAnchor.MiddleLeft;
                    _shelfSearchStyle.fontSize = 11;
                }
                return _shelfSearchStyle;
            }
        }

        private static GUIStyle _shelfItemButtonStyle = null;
        public static GUIStyle ShelfItemButtonStyle
        {
            get
            {
                if (_shelfItemButtonStyle == null)
                {
                    _shelfItemButtonStyle = new GUIStyle(EditorStyles.miniButtonMid);
                    _shelfItemButtonStyle.padding = new RectOffset(0, 0, 1, 1);
                    _shelfItemButtonStyle.border = new RectOffset(1, 1, 1, 1);
                    _shelfItemButtonStyle.imagePosition = ImagePosition.ImageOnly;
                    _shelfItemButtonStyle.wordWrap = true;
                    _shelfItemButtonStyle.normal.background = null;
                    _shelfItemButtonStyle.hover.background = ShelfStyles.ShelfItemButtonStyle.focused.background;
                }
                return _shelfItemButtonStyle;
            }
        }

        private static GUIStyle _shelfItemButtonTextStyle = null;
        public static GUIStyle ShelfItemButtonTextStyle
        {
            get
            {
                if (_shelfItemButtonTextStyle == null)
                {
                    _shelfItemButtonTextStyle = new GUIStyle(EditorStyles.miniLabel);
                    _shelfItemButtonTextStyle.imagePosition = ImagePosition.TextOnly;
                    _shelfItemButtonTextStyle.wordWrap = true;
                    _shelfItemButtonTextStyle.clipping = TextClipping.Clip;
                    _shelfItemButtonTextStyle.padding = new RectOffset(1, 1, 1, 1);
                    _shelfItemButtonTextStyle.border = new RectOffset(1, 1, 1, 1);
                }
                return _shelfItemButtonTextStyle;
            }
        }
        public static readonly Color ShelfItemButtonTextBackgroundPro = new Color(0.0f, 0.0f, 0.0f, 0.5f);
        public static readonly Color ShelfItemButtonTextBackgroundLite = new Color(1.0f, 1.0f, 1.0f, 0.5f);

        private static GUIStyle _shelfItemHeaderStyle = null;
        public static GUIStyle ShelfItemHeaderStyle
        {
            get
            {
                if (_shelfItemHeaderStyle == null)
                {
                    _shelfItemHeaderStyle = new GUIStyle(EditorStyles.miniLabel);
                    _shelfItemHeaderStyle.fontSize = 8;
                    _shelfItemHeaderStyle.fontStyle = FontStyle.Bold;
                }
                return _shelfItemHeaderStyle;
            }
        }
        public static readonly Color ShelfItemHeaderBackground = new Color(0.15f, 0.15f, 0.15f, 1.0f);

        private static GUIStyle _shelfItemGroupStyle = null;
        public static GUIStyle ShelfItemGroupStyle
        {
            get
            {
                if (_shelfItemGroupStyle == null)
                {
                    _shelfItemGroupStyle = new GUIStyle(EditorStyles.toolbarDropDown);
                    _shelfItemGroupStyle.fontSize = 9;
                    _shelfItemGroupStyle.fontStyle = FontStyle.Bold;
                    _shelfItemGroupStyle.alignment = TextAnchor.MiddleLeft;
                }
                return _shelfItemGroupStyle;
            }
        } // = new GUIStyle(EditorStyles.toolbarDropDown); // diversify into small and normal size (to have better control about font size and readability
        public static readonly Color ShelfItemGroupBackground = new Color(0, 0, 0, 0.0f);

        public static readonly GUILayoutOption OptExpandHeightON = GUILayout.ExpandHeight(true);
        public static readonly GUILayoutOption OptExpandWidthON = GUILayout.ExpandWidth(true);

        public static readonly GUILayoutOption OptExpandHeightOFF = GUILayout.ExpandHeight(false);
        public static readonly GUILayoutOption OptExpandWidthOFF = GUILayout.ExpandWidth(false);


        public static readonly float ShelfMenuSize = 24.0f;
        public static readonly float ShelfSeparatorSize = 5.0f;
        public static readonly float ShelfHeaderSize = 16.0f;
        public static readonly float ShelfGroupSize = 19.0f;
        public static readonly Dictionary<ShelfGUISize, float> ShelfButtonSize = new Dictionary<ShelfGUISize, float>()
        { { ShelfGUISize.Small, 32.0f }, { ShelfGUISize.Normal, 64.0f }};

        //// static ctor
        static ShelfStyles()
        {
            //ShelfPrefsNormalLeft.alignment = TextAnchor.MiddleLeft;
            //ShelfPrefsNormalLeft.fontSize = 11;

            //ShelfStyleNormal.alignment = TextAnchor.MiddleLeft;
            //ShelfStyleNormal.fixedHeight = ShelfStyles.ShelfMenuSize;
            //ShelfStyleNormal.fontSize = 11;
            //ShelfStyleNormal.fontStyle = FontStyle.Bold;

            //ShelfIconStyle.alignment = TextAnchor.MiddleCenter;
            //ShelfIconStyle.padding = new RectOffset(2, 1, 2, 1);
            //ShelfIconStyle.imagePosition = ImagePosition.ImageOnly;
            //ShelfIconStyle.fixedHeight = ShelfStyles.ShelfMenuSize;

            //ShelfStyle.alignment = TextAnchor.MiddleLeft;
            //ShelfStyleSelected.alignment = TextAnchor.MiddleLeft;
            //ShelfStyleSelected.normal = ShelfStyleSelected.onNormal;
            //ShelfStyleSelected.fixedHeight = ShelfStyles.ShelfMenuSize;
            //ShelfStyleSelected.fontSize = 11;
            //ShelfStyleSelected.fontStyle = FontStyle.Bold;

            //ShelfSearchStyle.fixedHeight = ShelfMenuSize - 4.0f;
            //ShelfSearchStyle.alignment = TextAnchor.MiddleLeft;
            ////ShelfSearchStyle.stretchHeight = true;
            //ShelfSearchStyle.fontSize = 11;

            //ShelfItemHeaderStyle.fontSize = 8;
            //ShelfItemHeaderStyle.fontStyle = FontStyle.Bold;

            //ShelfItemGroupStyle.fontSize = 9;
            //ShelfItemGroupStyle.fontStyle = FontStyle.Bold;
            //ShelfItemGroupStyle.alignment = TextAnchor.MiddleLeft;

            // styles for drawing ShelfButton objects
            //ShelfItemButtonStyle.padding = new RectOffset(0, 0, 1, 1);
            //ShelfItemButtonStyle.border = new RectOffset(1, 1, 1, 1);
            //// ! ! ! ! 
            //// add a style to include non-text version or alter it on the fly when choosing the small-size display option
            //// ! ! ! !
            //ShelfItemButtonStyle.imagePosition = ImagePosition.ImageOnly;
            //ShelfItemButtonStyle.wordWrap = true;
            //ShelfItemButtonStyle.normal.background = null;
            //ShelfItemButtonStyle.hover.background = ShelfItemButtonStyle.focused.background;

            //ShelfItemButtonTextStyle.alignment = TextAnchor.LowerLeft;
            //ShelfItemButtonTextStyle.imagePosition = ImagePosition.TextOnly;
            ////ShelfItemButtonTextStyle.wordWrap = true;
            //ShelfItemButtonTextStyle.clipping = TextClipping.Clip;
            //ShelfItemButtonTextStyle.padding = new RectOffset(1, 1, 1, 1);
            //ShelfItemButtonTextStyle.border = new RectOffset(1, 1, 1, 1);
        }

    }

    public class ShelfUtility
    {

        public static void DebugInfo()
        {
            Debug.Log("Shelves version 0.1 used");
        }

        /*
        [MenuItem("Extensions/Test")]
        public static void Test()
        {
            Type t = typeof(ShelfUtility);
            // ReflectMethod(t.FullName + ", " + t.Assembly.GetName().Name);
            //ReflectMethod("NoXP.Tools.Scan, Assembly-CSharp-Editor", "ShowWindow");
        }
        */

        public static void CallInternal(string internalPath)
        {

        }
        // parameters are only allowed with simple types, e.g.: int, float, string, Vector2, Vector3, etc.
        public static void ReflectMethod(string typeName, string methodName, object[] parameters = null)
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            Type type = Type.GetType(typeName, false, true);
            if (type != null)
            {
                try
                {
                    MethodInfo mi = type.GetMethod(methodName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.IgnoreReturn);
                    if (mi != null)
                        mi.Invoke(null, parameters);
                    // handle the case that the function is not found (either report back to the shelf class to disable the button/command or print warning/error)
                }
                catch (TargetParameterCountException tpcex)
                {
                    Debug.Log(string.Format(tpcex.Message + ". Check your XML configuration file to contain the correct amount of parameters for: '{0}' in '{1}'", methodName, typeName));
                }
            }
            else
                Debug.Log("No type found");
        }


        public static void Fallback()
        {
            Debug.Log("ShelfUtility.Fallback() method is called due to erroneous command configuration.");
        }

        // example method for reflection with UnityEngine-typed parameters
        public static void ResetTransform(Component[] transforms)
        {
            Undo.RecordObjects(transforms, "ResetTransforms");
            foreach (Transform tfs in transforms)
            {
                tfs.localPosition = Vector3.zero;
                tfs.localEulerAngles = Vector3.zero;
                tfs.localScale = Vector3.one;
            }
        }
        // example method for reflection with .NET typed multi parameter parameters
        public static void MathThingy(double a, double b, double c)
        {
            Debug.Log(a * b - c);
        }

        public static object ParseCommandParam(string typeString, string valueString)
        {
            switch (typeString.ToLowerInvariant())
            {
                case "string":
                    return valueString;
                case "int32":
                case "int":
                    return int.Parse(valueString, CultureInfo.InvariantCulture.NumberFormat);
                case "uint32":
                case "uint":
                    return uint.Parse(valueString, CultureInfo.InvariantCulture.NumberFormat);
                case "int16":
                case "short":
                    return short.Parse(valueString, CultureInfo.InvariantCulture.NumberFormat);
                case "uint16":
                case "ushort":
                    return ushort.Parse(valueString, CultureInfo.InvariantCulture.NumberFormat);
                case "sbyte":
                    return sbyte.Parse(valueString, CultureInfo.InvariantCulture.NumberFormat);
                case "byte":
                    return byte.Parse(valueString, CultureInfo.InvariantCulture.NumberFormat);
                case "single":
                case "float":
                    return float.Parse(valueString, CultureInfo.InvariantCulture.NumberFormat);
                case "double":
                    return double.Parse(valueString, CultureInfo.InvariantCulture.NumberFormat);
                case "decimal":
                    return decimal.Parse(valueString, CultureInfo.InvariantCulture.NumberFormat);
                case "special":
                    return ResolveCommandParamSpecial(valueString);
                case "selection":
                    return ResolveCommandParamSelection(valueString.Split('|'));
            }

            return null;
        }

        private static object ResolveCommandParamSpecial(string valueString)
        {

            return null;
        }
        private static Component[] ResolveCommandParamSelection(string[] selParams)
        {
            Type type = Type.GetType(selParams[0], false, true);
            //Debug.Log("Resolve: " + type + " from " + selParams[0]);

            List<Component> parameters = new List<Component>();
            if (typeof(Component).IsAssignableFrom(type))
            {
                if (selParams[1].ToLowerInvariant().Equals("first"))
                    parameters.Add(Selection.activeGameObject.GetComponent(type));
                else if (selParams[1].ToLowerInvariant().Equals("all"))
                {
                    List<object> all = new List<object>();
                    foreach (GameObject go in Selection.gameObjects)
                        parameters.AddRange(go.GetComponentsInChildren(type));
                }
            }
            //Debug.Log(parameters.Count);
            if (parameters.Count > 0)
                return parameters.ToArray();
            return null;
        }


        public static void DrawItems(List<ShelfItem> items, ref ShelfItemResponse response, ShelfGUIOrientation orientation, ShelfGUISize size)
        {
            if (orientation == ShelfGUIOrientation.Horizontal)
            {
                #region Draw with horizontal orientation
                using (new EditorGUILayout.HorizontalScope())
                {
                    //EditorGUILayout.BeginHorizontal();

                    if (size == ShelfGUISize.Small)
                    {
                        int lastIndex = items.Count;
                        bool groupOpen = false;
                        int groupStartIndex = 0;
                        GUILayoutOption maxWidth = GUILayout.MaxWidth(ShelfStyles.ShelfButtonSize[size]);
                        GUILayoutOption maxHeight = GUILayout.MaxHeight(Screen.height - 16.0f);

                        for (int i = 0; i < items.Count; i++)
                        {
                            Type itemType = items[i].GetType();
                            if (itemType.Equals(ShelfButton.Type))
                            {
                                if (!groupOpen)
                                {
                                    groupOpen = true;
                                    groupStartIndex = i;
                                    EditorGUILayout.BeginVertical(maxWidth, maxHeight); // ! ! ! ! solve this to use the new EditorGUIUtility.VerticalScope class
                                }

                                // change the default "Draw" to deferred execution draw using the ShelfItemResponse structures
                                items[i].Draw(ref response, orientation, size);

                                // check if group was opened and the index it was opened is different from the current index
                                // OR check if the current index is the last in the list (close vertical group when list ends)
                                if ((groupOpen && groupStartIndex != i)
                                    || i == lastIndex)
                                {
                                    EditorGUILayout.EndVertical();
                                    groupOpen = false;
                                }
                            }
                            else
                            {
                                if (groupOpen && groupStartIndex != i)
                                {
                                    EditorGUILayout.EndVertical();
                                    groupOpen = false;
                                }

                                // draw ShelfItem (Header or Separator, all non-Button types)
                                items[i].Draw(ref response, orientation, size);
                            }

                        }
                        /*
                        for (int i = 0; i < items.Count; i++)
                        {
                            Type itemType = items[i].GetType();
                            if (!itemType.Equals(ShelfButton.Type))
                            {
                                if (groupOpen && groupStartIndex != i)
                                {
                                    EditorGUILayout.EndVertical();
                                    groupOpen = false;
                                }

                                // draw ShelfItem (Header or Separator, all non-Button types)
                                items[i].Draw(ref response, orientation, size);
                            }
                            else
                            {
                                if (!groupOpen)
                                {
                                    groupOpen = true;
                                    groupStartIndex = i;
                                    EditorGUILayout.BeginVertical(maxWidth, maxHeight); // ! ! ! ! solve this to use the new EditorGUIUtility.VerticalScope class
                                }

                                // change the default "Draw" to deferred execution draw using the ShelfItemResponse structures
                                items[i].Draw(ref response, orientation, size);

                                // check if group was opened and the index it was opened is different from the current index
                                // OR check if the current index is the last in the list (close vertical group when list ends)
                                if ((groupOpen && groupStartIndex != i)
                                    || i == lastIndex)
                                {
                                    EditorGUILayout.EndVertical();
                                    groupOpen = false;
                                }
                            }
                        }
                        */

                        if (groupOpen)
                        {
                            groupOpen = false;
                            EditorGUILayout.EndVertical();
                        }
                    }
                    else
                    {
                        // ! ! ! ! 
                        // shares the 1:1 same code with the vertical orientation normal drawing of ShelfElements
                        foreach (ShelfItem item in items)
                            item.Draw(ref response, orientation, size);
                    }

                    //EditorGUILayout.EndHorizontal();
                }


                #endregion
            }
            else
            {
                #region Draw with vertical orientation
                //EditorGUILayout.BeginVertical(GUILayout.MaxHeight(Screen.height - 16.0f), ShelfStyles.OptExpandHeightOFF);
                using (new EditorGUILayout.VerticalScope(GUILayout.MaxHeight(Screen.height - 16.0f), ShelfStyles.OptExpandHeightOFF))
                {

                    if (size == ShelfGUISize.Small)
                    {
                        int lastIndex = items.Count;
                        bool groupOpen = false;
                        int groupStartIndex = 0;

                        for (int i = 0; i < items.Count; i++)
                        {
                            Type itemType = items[i].GetType();

                            if (items[i].GetType().Equals(ShelfButton.Type))
                            {
                                if (!groupOpen)
                                {
                                    groupOpen = true;
                                    groupStartIndex = i;
                                    EditorGUILayout.BeginHorizontal();
                                }

                                // change the default "Draw" to deferred execution draw using the ShelfItemResponse structures
                                items[i].Draw(ref response, orientation, size);

                                // check if group was opened and the index it was opened is different from the current index
                                // OR check if the current index is the last in the list (close vertical group when list ends)
                                if ((groupOpen && groupStartIndex != i)
                                    || i == lastIndex)
                                {
                                    EditorGUILayout.EndHorizontal();
                                    groupOpen = false;
                                }
                            }
                            else
                            {
                                if (groupOpen && groupStartIndex != i)
                                {
                                    EditorGUILayout.EndHorizontal();
                                    groupOpen = false;
                                }
                                // draw ShelfItem (Header or Separator, all non-Button types)
                                items[i].Draw(ref response, orientation, size);
                            }
                        }

                        if (groupOpen)
                        {
                            groupOpen = false;
                            EditorGUILayout.EndHorizontal();
                        }
                    }
                    else
                    {
                        foreach (ShelfItem item in items)
                            item.Draw(ref response, orientation, size);
                    }

                    //EditorGUILayout.EndVertical();
                }
                #endregion
            }


        }

        public static List<ShelfItem> ReadShelfItems(XElement elBase, XNamespace xNs)
        {
            List<ShelfItem> results = new List<ShelfItem>();

            List<XElement> elements = new List<XElement>();
            elements.AddRange(elBase.Elements());

            // iterate over all shelf items (buttons and separators)
            //foreach (XElement elItem in elBase.Elements(xNs + "Item"))
            foreach (XElement elItem in elements)
            {
                // check if the item is a 'Item'
                if (elItem.Name.LocalName.Equals("Item"))
                {
                    XAttribute attrItemType = elItem.Attribute("type");
                    if (attrItemType != null)
                    {
                        if (attrItemType.Value.Equals(ShelfItem.Types.Separator.ToString()))
                            results.Add(ShelfSeparator.Instance);
                        else if (attrItemType.Value.Equals(ShelfItem.Types.Header.ToString()))
                        {
                            XAttribute attrItemName = elItem.Attribute("name");
                            if (attrItemName != null) results.Add(new ShelfHeader(attrItemName.Value));
                        }
                        else
                        {

                            IEnumerable<XElement> elCommands = elItem.Elements(xNs + "Command");
                            XAttribute attrItemName = elItem.Attribute("name");
                            XAttribute attrItemTooltip = elItem.Attribute("tooltip");
                            XAttribute attrItemIconPath = elItem.Attribute("iconPath");

                            ShelfButton item = new ShelfButton((attrItemName != null ? attrItemName.Value : "Shelf" + results.Count),
                                                                (attrItemTooltip != null ? attrItemTooltip.Value : string.Empty),
                                                                (attrItemIconPath != null ? attrItemIconPath.Value : string.Empty));

                            // iterate over all shelf items (buttons and separators)
                            foreach (XElement elCommand in elCommands)
                            {
                                XAttribute attrCmdType = elCommand.Attribute("type");
                                XAttribute attrCmdPath = elCommand.Attribute("path");

                                CommandType type = (attrCmdType != null ? (CommandType)Enum.Parse(typeof(CommandType), attrCmdType.Value) : CommandType.MenuItem);
                                string path = (attrCmdType != null ? attrCmdPath.Value : string.Empty);

                                if (path.Equals(string.Empty))
                                {
                                    type = CommandType.Internal;
                                    path = "NoXP.ShelfUtility, Assembly-CSharp-Editor;Fallback";
                                }

                                Command cmd = new Command(type, path);

                                foreach (XElement elCmdParam in elCommand.Elements(xNs + "CommandParam"))
                                {
                                    XAttribute attrCmdParamValue = elCmdParam.Attribute("value");
                                    XAttribute attrCmdParamType = elCmdParam.Attribute("valueType");
                                    if (attrCmdParamValue != null && !attrCmdParamValue.Value.Equals(string.Empty)
                                        && attrCmdParamType != null && !attrCmdParamType.Value.Equals(string.Empty))
                                        cmd.AddParamDefinition(new CommandParam(attrCmdParamType.Value, attrCmdParamValue.Value));
                                }

                                item.Add(cmd);
                            }

                            results.Add(item);
                        }
                    }
                }
                else if (elItem.Name.LocalName.Equals("Group"))
                {
                    int groupID = 0;
                    // iterate over all shelf items (buttons and separators)
                    //foreach (XElement elGroup in elItem.Elements(xNs + "Group"))
                    //{
                    XAttribute attrItemName = elItem.Attribute("name");
                    XAttribute attrItemTooltip = elItem.Attribute("tooltip");
                    ShelfGroup group = new ShelfGroup((attrItemName != null ? attrItemName.Value : "Group" + groupID),
                                                        (attrItemTooltip != null ? attrItemTooltip.Value : string.Empty));
                    // get the shelf items contained inside the shelf group
                    group.AddRange(ShelfUtility.ReadShelfItems(elItem, xNs));
                    // add shelf group to the shelf
                    results.Add(group);
                    groupID++;
                    //}

                }
            }

            return results;
        }



    }

}