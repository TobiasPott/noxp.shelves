﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace NoXP.Shelves
{
    public enum CommandType
    {
        MenuItem,
        Internal,
        External
    }

    // add a ShelfGUISize.Minimal
    // -> this should draw the shelf item as text only (without any icons)
    // -> using always ShelfGUISize.Normal width and one (maximum two, perhaps three) lines of text to display the name)

    public enum ShelfGUISize
    {
        Small = 0,
        Normal = 1, // default value
        Minimal = 2, // text only mode
    }

    public enum ShelfGUIOrientation
    {
        Horizontal = 0,
        Vertical = 1
    }

    public struct CommandParam
    {
        private string _valueType;
        private string _value;

        public string Value
        { get { return _value; } }
        public string ValueType
        { get { return _valueType; } }

        public CommandParam(string valueType, string value)
        {
            _value = value;
            _valueType = valueType;
        }
    }

    public class Command
    {
        public const string PrefixMenuItem = "menuitem:";
        public const string PrefixInternal = "internal:";
        public const string PrefixExternal = "external:";

        private CommandType _type;
        private string _value;
        private List<object> _parameters = new List<object>();
        private List<CommandParam> _paramDefinitions = new List<CommandParam>();

        public CommandType Type
        { get { return _type; } }
        public string Value
        { get { return _value; } }


        internal Command(CommandType type, string value)
        {
            _type = type;
            _value = value;
        }

        // creator methods
        public static Command MenuItem(string menuItemPath)
        { return new Command(CommandType.MenuItem, menuItemPath); }
        public static Command Internal(string commandValue)
        { return new Command(CommandType.Internal, commandValue); }
        public static Command external(string commandValue)
        { return new Command(CommandType.External, commandValue); }


        public void AddParamDefinition(CommandParam param)
        {
            _paramDefinitions.Add(param);
        }
        public void RemoveParamDefinition(CommandParam param)
        {
            _paramDefinitions.Remove(param);
        }
        public void ClearParamDefinition()
        {
            _paramDefinitions.Clear();
        }

        public void AddParameter(object param)
        {
            _parameters.Add(param);
        }
        public void RemoveParameter(object param)
        {
            _parameters.Remove(param);
        }
        public void ClearParameters()
        {
            _parameters.Clear();
        }

        public void Execute()
        {
            // parse parameters from paramDefinition list
            this.ClearParameters();
            foreach (CommandParam param in _paramDefinitions)
            {
                if (!param.Value.Equals(string.Empty) && !param.ValueType.Equals(string.Empty))
                    this.AddParameter(ShelfUtility.ParseCommandParam(param.ValueType, param.Value));
            }

            switch (_type)
            {
                case CommandType.MenuItem:
                    this.ExecuteMenuItem(_value.Replace(Command.PrefixMenuItem, string.Empty));
                    break;
                case CommandType.Internal:
                    this.ExecuteInternal(_value.Replace(Command.PrefixInternal, string.Empty));
                    break;
                case CommandType.External:
                    this.ExecuteExternal(_value.Replace(Command.PrefixExternal, string.Empty));
                    break;
            }

        }
        private void ExecuteMenuItem(string menuItemPath)
        {
            EditorApplication.ExecuteMenuItem(menuItemPath);
        }
        private void ExecuteInternal(string command)
        {
            string[] splitted = command.Split(';');
            ShelfUtility.ReflectMethod(splitted[0], splitted[1], (_parameters.Count != 0 ? _parameters.ToArray() : null));
        }
        private void ExecuteExternal(string command)
        {
            Debug.LogWarning("Execution of external methods is not implemented yet. Would have called: " + command);
        }

    }

    public struct ShelfItemResponse
    {
        // empty response 
        public static ShelfItemResponse Empty
        { get { return new ShelfItemResponse(null, false, -1, EventModifiers.None); } }

        public static ShelfItemResponse Current = ShelfItemResponse.Empty;

        private ShelfItem _item;
        private bool _triggered;
        private int _mouseButton;
        private EventModifiers _modifiers;
        private int _clickCount;

        public ShelfItem Item
        {
            get { return _item; }
            set { _item = value; }
        }
        public bool Triggered
        {
            get { return _triggered; }
            set { _triggered = value; }
        }
        public int MouseButton
        {
            get { return _mouseButton; }
            //set { _mouseButton = value; }
        }
        public EventModifiers Modifiers
        {
            get { return _modifiers; }
            //set { _modifiers = value; }
        }
        //public int ClickCount
        //{
        //    get { return _clickCount; }
        //    //set { _clickCount = value; }
        //}

        public ShelfItemResponse(ShelfItem item, bool triggered, int mouseButton, EventModifiers modifiers = EventModifiers.None)
        {
            _item = item;
            _triggered = triggered;
            _mouseButton = mouseButton;
            _modifiers = modifiers;
            _clickCount = 0;
        }

        public void Reset()
        {
            _item = null;
            _triggered = false;
            _mouseButton = -1;
            _modifiers = EventModifiers.None;
            _clickCount = 0;
        }

        public void SetData(Event ev)
        {
            _mouseButton = ev.button;
            _modifiers = ev.modifiers;
            _clickCount = ev.clickCount;
        }

        public override string ToString()
        {
            return "ShelfItemResponse: {" + this.Item + "}, MouseButton: {" + this.MouseButton + "}, Modifiers: {" + this.Modifiers + "}";
        }
    }

    public class ShelfResources
    {
        private static string EditorDefaultResources = "Editor Default Resources/";
        private static string BasePath = "Assets/Editor/NoXP.Shelves";
        //private static string BasePath = "Shelves/";
        private static string SubPath_UI = "/UI";

        public static readonly string[] ShelvesNamesFiller = new string[] { "" };


        public static T LoadAsset<T>(string path, bool editorDefaultResources = false) where T : UnityEngine.Object
        {
            if (editorDefaultResources)
                return (T)EditorGUIUtility.Load(path);
            else
                return AssetDatabase.LoadAssetAtPath<T>(path);

        }
        public static T LoadAssetAuto<T>(string path) where T : UnityEngine.Object
        {
            if (path.StartsWith(ShelfResources.EditorDefaultResources))
                return (T)EditorGUIUtility.Load(path.Substring(ShelfResources.EditorDefaultResources.Length));
            else
                return AssetDatabase.LoadAssetAtPath<T>(path);

        }



        private static Texture2D _uires_Refresh = null;
        public static Texture2D UIRES_Refresh
        {
            get
            {
                if (_uires_Refresh == null)
                    //_uires_Refresh = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_Refresh.png");
                    _uires_Refresh = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_Refresh.png");
                return _uires_Refresh;
            }
        }
        private static Texture2D _uires_Refresh2 = null;
        public static Texture2D UIRES_Refresh2
        {
            get
            {
                if (_uires_Refresh2 == null)
                    //_uires_Refresh2 = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_Refresh2.png");
                    _uires_Refresh2 = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_Refresh2.png");
                return _uires_Refresh2;
            }
        }
        private static Texture2D _uires_Settings = null;
        public static Texture2D UIRES_Settings
        {
            get
            {
                if (_uires_Settings == null)
                    //_uires_Settings = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_Settings.png");
                    _uires_Settings = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_Settings.png");
                return _uires_Settings;
            }
        }
        private static Texture2D _uires_Search = null;
        public static Texture2D UIRES_Search
        {
            get
            {
                if (_uires_Search == null)
                    //_uires_Search = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_Search.png");
                    _uires_Search = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_Search.png");
                return _uires_Search;
            }
        }

        private static Texture2D _uires_OrientH = null;
        public static Texture2D UIRES_OrientH
        {
            get
            {
                if (_uires_OrientH == null)
                    //_uires_OrientH = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_OrientH.png");
                    _uires_OrientH = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_OrientH.png");
                return _uires_OrientH;
            }
        }
        private static Texture2D _uires_OrientV = null;
        public static Texture2D UIRES_OrientV
        {
            get
            {
                if (_uires_OrientV == null)
                    //_uires_OrientV = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_OrientV.png");
                    _uires_OrientV = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_OrientV.png");
                return _uires_OrientV;
            }
        }

        private static Texture2D _uires_SizeSmall = null;
        public static Texture2D UIRES_SizeSmall
        {
            get
            {
                if (_uires_SizeSmall == null)
                    //_uires_SizeSmall = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_SizeSmall.png");
                    _uires_SizeSmall = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_SizeSmall.png");
                return _uires_SizeSmall;
            }
        }
        private static Texture2D _uires_SizeNormal = null;
        public static Texture2D UIRES_SizeNormal
        {
            get
            {
                if (_uires_SizeNormal == null)
                    //_uires_SizeNormal = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_SizeNormal.png");
                    _uires_SizeNormal = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_SizeNormal.png");
                return _uires_SizeNormal;
            }
        }
        private static Texture2D _uires_Edit = null;
        public static Texture2D UIRES_Edit
        {
            get
            {
                if (_uires_Edit == null)
                    //_uires_Edit = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_Edit.png");
                    _uires_Edit = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_Edit.png");
                return _uires_Edit;
            }
        }
        private static Texture2D _uires_EditShelf = null;
        public static Texture2D UIRES_EditShelf
        {
            get
            {
                if (_uires_EditShelf == null)
                    //_uires_EditShelf = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_EditShelf.png");
                    _uires_EditShelf = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_EditShelf.png");
                return _uires_EditShelf;
            }
        }

        private static Texture2D _uires_TrashOpened = null;
        public static Texture2D UIRES_TrashOpened
        {
            get
            {
                if (_uires_TrashOpened == null)
                    //_uires_TrashOpened = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_TrashOpened.png");
                    _uires_TrashOpened = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_TrashOpened.png");
                return _uires_TrashOpened;
            }
        }
        private static Texture2D _uires_TrashClosed = null;
        public static Texture2D UIRES_TrashClosed
        {
            get
            {
                if (_uires_TrashClosed == null)
                    //_uires_TrashClosed = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_TrashClosed.png");
                    _uires_TrashClosed = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_TrashClosed.png");
                return _uires_TrashClosed;
            }
        }
        private static Texture2D _uires_Cancel = null;
        public static Texture2D UIRES_Cancel
        {
            get
            {
                if (_uires_Cancel == null)
                    //_uires_Cancel = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_Cancel.png");
                    _uires_Cancel = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_Cancel.png");
                return _uires_Cancel;
            }
        }

        private static Texture2D _uires_VisibleOn = null;
        public static Texture2D UIRES_VisibleOn
        {
            get
            {
                if (_uires_VisibleOn == null)
                    //_uires_VisibleOn = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_VisibleOn.png");
                    _uires_VisibleOn = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_VisibleOn.png");
                return _uires_VisibleOn;
            }
        }
        private static Texture2D _uires_VisibleOff = null;
        public static Texture2D UIRES_VisibleOff
        {
            get
            {
                if (_uires_VisibleOff == null)
                    //_uires_VisibleOff = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_VisibleOff.png");
                    _uires_VisibleOff = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_VisibleOff.png");
                return _uires_VisibleOff;
            }
        }
        private static Texture2D _uires_VisibleAll = null;
        public static Texture2D UIRES_VisibleAll
        {
            get
            {
                if (_uires_VisibleAll == null)
                    //_uires_VisibleAll = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_VisibleAll.png");
                    _uires_VisibleAll = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_VisibleAll.png");
                return _uires_VisibleAll;
            }
        }

        private static Texture2D _uires_ListActive = null;
        public static Texture2D UIRES_ListActive
        {
            get
            {
                if (_uires_ListActive == null)
                    //_uires_ListActive = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_ListActive.png");
                    _uires_ListActive = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_ListActive.png");
                return _uires_ListActive;
            }
        }
        private static Texture2D _uires_ListInactive = null;
        public static Texture2D UIRES_ListInactive
        {
            get
            {
                if (_uires_ListInactive == null)
                    //_uires_ListInactive = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_ListInactive.png");
                    _uires_ListInactive = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_ListInactive.png");
                return _uires_ListInactive;
            }
        }

        private static Texture2D _uires_CheckBox_Checked = null;
        public static Texture2D UIRES_CheckBox_Checked
        {
            get
            {
                if (_uires_CheckBox_Checked == null)
                    //_uires_CheckBox_Checked = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_CheckBox_Checked.png");
                    _uires_CheckBox_Checked = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_CheckBox_Checked.png");
                return _uires_CheckBox_Checked;
            }
        }
        private static Texture2D _uires_CheckBox_Unchecked = null;
        public static Texture2D UIRES_CheckBox_Unchecked
        {
            get
            {
                if (_uires_CheckBox_Unchecked == null)
                    //_uires_CheckBox_Unchecked = (Texture2D)EditorGUIUtility.Load(BasePath + SubPath_UI + "/UIRES_CheckBox_Unchecked.png");
                    _uires_CheckBox_Unchecked = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/UIRES_CheckBox_Unchecked.png");
                return _uires_CheckBox_Unchecked;
            }
        }

        // GUIContent objects
        private static GUIContent _guicont_Add = null;
        public static GUIContent GUICONT_Add
        {
            get
            {
                if (_guicont_Add == null)
                    _guicont_Add = new GUIContent("...", "Load shelves from an existing configuration.");
                return _guicont_Add;
            }
        }
        private static GUIContent _guicont_New = null;
        public static GUIContent GUICONT_New
        {
            get
            {
                if (_guicont_New == null)
                    _guicont_New = new GUIContent("+", "Add a new blank shelf.");
                return _guicont_New;
            }
        }
        private static GUIContent _guicont_Refresh = null;
        public static GUIContent GUICONT_Refresh
        {
            get
            {
                if (_guicont_Refresh == null)
                    _guicont_Refresh = new GUIContent(UIRES_Refresh, "Refresh all shelves");
                return _guicont_Refresh;
            }
        }
        private static GUIContent _guicont_Settings = null;
        public static GUIContent GUICONT_Settings
        {
            get
            {
                if (_guicont_Settings == null)
                    _guicont_Settings = new GUIContent(UIRES_Settings, "Open settings & options");
                return _guicont_Settings;
            }
        }
        private static GUIContent _guicont_Search = null;
        public static GUIContent GUICONT_Search
        {
            get
            {
                if (_guicont_Search == null)
                    _guicont_Search = new GUIContent(UIRES_Search, "Open search field");
                return _guicont_Search;
            }
        }

        private static GUIContent _guicont_OrientH = null;
        public static GUIContent GUICONT_OrientH
        {
            get
            {
                if (_guicont_OrientH == null)
                    _guicont_OrientH = new GUIContent(UIRES_OrientH, "Horizontal orientation");
                return _guicont_OrientH;
            }
        }
        private static GUIContent _guicont_OrientV = null;
        public static GUIContent GUICONT_OrientV
        {
            get
            {
                if (_guicont_OrientV == null)
                    _guicont_OrientV = new GUIContent(UIRES_OrientV, "Vertical orientation");
                return _guicont_OrientV;
            }
        }

        private static GUIContent _guicont_SizeSmall = null;
        public static GUIContent GUICONT_SizeSmall
        {
            get
            {
                if (_guicont_SizeSmall == null)
                    _guicont_SizeSmall = new GUIContent(UIRES_SizeSmall, "Small icon size");
                return _guicont_SizeSmall;
            }
        }
        private static GUIContent _guicont_SizeNormal = null;
        public static GUIContent GUICONT_SizeNormal
        {
            get
            {
                if (_guicont_SizeNormal == null)
                    _guicont_SizeNormal = new GUIContent(UIRES_SizeNormal, "Normal icon size");
                return _guicont_SizeNormal;
            }
        }

    }

}