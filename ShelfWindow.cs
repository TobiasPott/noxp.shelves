﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;

namespace NoXP.Shelves
{
    // [InitializeOnLoad]
    public class ShelfWindow : EditorWindow
    {

        [SerializeField()]
        private ShelfGUISize _guiSize = ShelfGUISize.Normal;
        [SerializeField()]
        private ShelfGUIOrientation _guiOrientation = ShelfGUIOrientation.Horizontal;

        [SerializeField()]
        private int _activeShelf = 0;
        [SerializeField()]
        private string[] _cfgFiles = new string[0];

        private bool _orientationChanged = false;
        private List<Shelf> _shelves = new List<Shelf>();

        [SerializeField()]
        private Vector2 _prevSizeH = new Vector2(800.0f, 104.0f);
        [SerializeField()]
        private Vector2 _prevSizeV = new Vector2(104.0f, 800.0f);

        private XElement _xmlConfigNode = null;

        private static List<ShelfWindow> _instances = new List<ShelfWindow>();


        public XElement ConfigNode
        {
            get { return _xmlConfigNode; }
        }


        public static void ClearInstances()
        {
            for (int i = 0; i < _instances.Count; i++)
            {
                if (_instances[i] != null)
                    GameObject.DestroyImmediate(_instances[i]);
            }
        }
        public static void ClearShelves()
        {
            ShelfWindow[] windows = Resources.FindObjectsOfTypeAll<ShelfWindow>();
            for (int i = 0; i < windows.Length; i++)
            {
                // close the window directly without changing the isVisible flag
                if (windows[i] != null)
                    windows[i].Close();
                GameObject.DestroyImmediate(windows[i]);
            }
        }
        public static void LoadShelfFromOpenDialog(bool clear = true)
        {
            string xmlPath = EditorUtility.OpenFilePanel("Select shelf configuration file..", Application.dataPath, "scfg");
            ShelfPrefs.SaveConfig();
            ShelfPrefs.LoadConfig(xmlPath);
            LoadShelfFromXDocument(ShelfPrefs.XmlConfig, clear);
        }
        public static void LoadShelf(bool clear = true)
        {
            // ShelfPrefs.SaveConfig();
            ShelfPrefs.UpdateConfigPath();
            ShelfPrefs.ReloadConfig();

            LoadShelfFromXDocument(ShelfPrefs.XmlConfig, clear);
        }
        public static void LoadShelfFromXDocument(XDocument xmlDoc, bool clear = true)
        {
            if (xmlDoc != null)
            {
                try
                {
                    //if (clear)
                    //    // clear all prior existing shelves
                    //    ShelfWindow.ClearShelves();

                    XNamespace xNs = "http://tempuri.org/ShelvesCFG.xsd";

                    // iterate over all shelf nodes in the xml document
                    foreach (XElement elWindow in xmlDoc.Root.Elements(xNs + "Window"))
                    {
                        // get visibility attribute and check for early exit condition (not visible, thus window creation not requried)
                        XAttribute attrVisible = elWindow.Attribute("visible");
                        bool vVisible = attrVisible != null ? bool.Parse(attrVisible.Value) : true;
                        if (!vVisible)
                            continue;

                        XAttribute attrTitle = elWindow.Attribute("title");
                        string vTitle = attrTitle != null ? attrTitle.Value : string.Empty;
                        XAttribute attrOrientation = elWindow.Attribute("orientation");
                        int vOrientation = attrOrientation != null ? int.Parse(attrOrientation.Value) : 0;
                        XAttribute attrSize = elWindow.Attribute("size");
                        int vSize = attrSize != null ? int.Parse(attrSize.Value) : 0;
                        XAttribute attrActiveIndex = elWindow.Attribute("activeIndex");
                        int vActiveIndex = attrActiveIndex != null ? int.Parse(attrActiveIndex.Value) : 0;

                        XAttribute attrRectX, attrRectY, attrRectWidth, attrRectHeight;
                        // parse data for horizontal position rectangle
                        XElement elRectH = elWindow.Element(xNs + "RectH");
                        attrRectX = elRectH.Attribute("x");
                        attrRectY = elRectH.Attribute("y");
                        attrRectWidth = elRectH.Attribute("width");
                        attrRectHeight = elRectH.Attribute("height");
                        Rect vRectH = new Rect();
                        vRectH.x = attrRectX != null ? float.Parse(attrRectX.Value) : 0.0f;
                        vRectH.y = attrRectY != null ? float.Parse(attrRectY.Value) : 0.0f;
                        vRectH.width = attrRectWidth != null ? float.Parse(attrRectWidth.Value) : 0.0f;
                        vRectH.height = attrRectHeight != null ? float.Parse(attrRectHeight.Value) : 0.0f;

                        // parse data for vertical position rectangle
                        XElement elRectV = elWindow.Element(xNs + "RectV");
                        attrRectX = elRectV.Attribute("x");
                        attrRectY = elRectV.Attribute("y");
                        attrRectWidth = elRectV.Attribute("width");
                        attrRectHeight = elRectV.Attribute("height");
                        Rect vRectV = new Rect();
                        vRectV.x = attrRectX != null ? float.Parse(attrRectX.Value) : 0.0f;
                        vRectV.y = attrRectY != null ? float.Parse(attrRectY.Value) : 0.0f;
                        vRectV.width = attrRectWidth != null ? float.Parse(attrRectWidth.Value) : 0.0f;
                        vRectV.height = attrRectHeight != null ? float.Parse(attrRectHeight.Value) : 0.0f;

                        XElement elConfig = elWindow.Element(xNs + "Configuration");

                        IEnumerable<XElement> elsFiles = elConfig.Elements(xNs + "File");
                        //string[] cfgFiles = new string[elsFiles.Count()];
                        List<string> cfgFiles = new List<string>();
                        int cfgInd = 0;
                        foreach (XElement elFile in elsFiles)
                        {
                            Debug.Log(elFile.Value);
                            if (File.Exists(elFile.Value))
                            {
                                cfgFiles.Add(elFile.Value);
                                cfgInd++;
                            }
                        }

                        // create window and add it to the instance list
                        ShelfWindow window = CreateNewWithRect(vTitle, vRectH, vRectV, (ShelfGUIOrientation)vOrientation, (ShelfGUISize)vSize, cfgFiles.ToArray());
                        window._activeShelf = vActiveIndex;
                        window.SetXmlConfigNode(elWindow);
                        _instances.Add(window);
                    }

                }
                catch (Exception ex)
                {
                    // print exception to console if any occured
                    Debug.LogWarning("Exception occured during loading of xml configuration: " + ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
            if (xmlDoc == null)
            {
                ShelfWindow.ClearShelves();
            }
        }

        private static ShelfWindow CreateNewWithRect(string title, Rect rectH, Rect rectV, ShelfGUIOrientation orientation, ShelfGUISize size, string[] cfgFiles)
        {
            ShelfWindow window = null;
            ShelfWindow[] allInstances = Resources.FindObjectsOfTypeAll<ShelfWindow>();
            foreach (ShelfWindow inst in allInstances)
            {
                if (inst.titleContent.text.Equals(title))
                {
                    window = inst;
                    break;
                }
            }

            if (window == null)
            {
                Debug.Log("Create new ShelfWindow");
                window = EditorWindow.CreateInstance<ShelfWindow>();
                window.titleContent = new GUIContent(title);
                window.position = orientation == ShelfGUIOrientation.Horizontal ? rectH : rectV;
                window.autoRepaintOnSceneChange = true;
            }

            window.Show();
            window.LoadShelves(cfgFiles);
            window.SetSizes(rectH, rectV);
            window.SwitchOrientation(orientation, true);
            window.SwitchSize(size);
            window.Refresh();
            window.Focus();
            return window;
        }

        private void SetXmlConfigNode(XElement node)
        {
            _xmlConfigNode = node;
        }

        private void SetSizes(Rect horizontal, Rect vertical)
        {
            _prevSizeH = horizontal.size;
            _prevSizeV = vertical.size;
        }

        bool _isVisible = true;
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                if (_xmlConfigNode != null)
                    _xmlConfigNode.Attribute("visible").SetValue(_isVisible.ToString().ToLowerInvariant());
                if (!_isVisible)
                    this.Close();
            }
        }

        private void OnDestroy()
        {
            // check if xml node is set and store the ShelfWindows data
            if (_xmlConfigNode != null)
            {
                // write back values to the xml node
                this.SaveWindowCFG();
                // remove destroyed window from instances list
                _instances.Remove(this);
                // if it was the last instance destroyed, save all changes back to the CFG file
                //if (_instances.Count <= 0)
                ShelfPrefs.SaveConfig();
            }
            // call to ShelfPrefs to refresh it's data 
            ShelfPrefs.Refresh();
        }

        public void SaveWindowCFG()
        {
            XNamespace xNs = "http://tempuri.org/ShelvesCFG.xsd";

            _xmlConfigNode.Attribute("title").SetValue(this.titleContent.text);
            _xmlConfigNode.Attribute("orientation").SetValue((int)this._guiOrientation);
            _xmlConfigNode.Attribute("size").SetValue((int)this._guiSize);
            _xmlConfigNode.Attribute("activeIndex").SetValue(this._activeShelf);

            // parse data for horizontal position rectangle
            XElement elRectH = _xmlConfigNode.Element(xNs + "RectH");
            elRectH.Attribute("x").SetValue(this.position.x);
            elRectH.Attribute("y").SetValue(Mathf.Clamp(this.position.y - 5, 10, int.MaxValue)); // dunno why I need to subtract 5 of the y-position, perhabs this is a windows 10 issue
            elRectH.Attribute("width").SetValue((_guiOrientation == ShelfGUIOrientation.Horizontal ? this.position.width : this._prevSizeH.x));
            elRectH.Attribute("height").SetValue(Mathf.Min(104.0f, (_guiOrientation == ShelfGUIOrientation.Horizontal ? this.position.height : this._prevSizeH.y)));

            // parse data for vertical position rectangle
            XElement elRectV = _xmlConfigNode.Element(xNs + "RectV");
            elRectV.Attribute("x").SetValue(this.position.x);
            elRectV.Attribute("y").SetValue(Mathf.Clamp(this.position.y - 5, 10, int.MaxValue)); // dunno why I need to subtract 5 of the y-position, perhabs this is a windows 10 issue
            elRectV.Attribute("width").SetValue(Mathf.Min(104.0f, (_guiOrientation == ShelfGUIOrientation.Vertical ? this.position.width : this._prevSizeV.x)));
            elRectV.Attribute("height").SetValue((_guiOrientation == ShelfGUIOrientation.Vertical ? this.position.height : this._prevSizeV.y));

            XElement elConfig = _xmlConfigNode.Element(xNs + "Configuration");
            elConfig.RemoveAll();
            // _cfgFiles
            foreach (string cfgFile in _cfgFiles)
            {
                if (!cfgFile.Equals(string.Empty) && File.Exists(cfgFile))
                {
                    XElement elFile = new XElement(xNs + "File");
                    elFile.Value = cfgFile;
                    elConfig.Add(elFile);
                }
            }
        }


        private void LoadShelves(string[] cfgFiles)
        {
            Array.Resize<string>(ref _cfgFiles, cfgFiles.Length);
            Array.Copy(cfgFiles, _cfgFiles, cfgFiles.Length);

            this.ReloadShelves();
        }
        private void ReloadShelves()
        {
            _shelves.Clear();
            foreach (string cfgFile in _cfgFiles)
                ShelfWindow.ReadShelfConfig(cfgFile, ref _shelves);
        }

        void OnInspectorUpdate()
        {
            // check if config is set or not (if not load shelfes from last recent file)
            //if (_xmlConfig == null)
            //    ShelfWindow.LoadShelf();

            if (_orientationChanged)
            {
                if (_guiOrientation == ShelfGUIOrientation.Horizontal)
                {
                    this.minSize = new Vector2(800.0f, _prevSizeH.y);
                    this.maxSize = new Vector2(2048.0f, _prevSizeH.y);
                }
                else
                {
                    this.minSize = new Vector2(_prevSizeV.x, 800.0f);
                    this.maxSize = new Vector2(_prevSizeV.x, 2048.0f);
                }
                _orientationChanged = false;
            }

        }

        void OnGUI()
        {

            if (_shelves.Count == 0)
                this.ReloadShelves();

            ShelfItemResponse response = ShelfItemResponse.Current;
            response.Reset();

            if (_guiOrientation == ShelfGUIOrientation.Horizontal)
                this.OnGUIHorizontal(ref response, _guiSize);
            else
                this.OnGUIVertical(ref response, _guiSize);

            #region Deferred UI Event Handling
            // Deferred UI Event Handling
            if (response.Item != null)
            {
                //Debug.Log(response.ToString());
                if (response.Triggered)
                {
                    if (Event.current.button == 0)
                        response.Item.ExecuteCommands();
                }
            }
            #endregion

            #region Icon extraction
            /*
            EditorGUI.DrawRect(new Rect(0, 0, 2048, 2048), Color.white);
            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(1538));
            // AssetPreview.GetMiniTypeThumbnail(compType);
            //GUILayout.Label(AssetPreview.GetMiniTypeThumbnail(typeof(AudioReverbZone)));
            DisplayIcon<NetworkAnimator>();
            DisplayIcon<NetworkDiscovery>();
            DisplayIcon<NetworkIdentity>();

            DisplayIcon<NetworkLobbyManager>();
            DisplayIcon<NetworkLobbyPlayer>();
            DisplayIcon<NetworkManager>();
            DisplayIcon<NetworkManagerHUD>();
            DisplayIcon<NetworkMigrationManager>();
            DisplayIcon<NetworkProximityChecker>();
            DisplayIcon<NetworkStartPosition>();
            DisplayIcon<NetworkTransform>();
            DisplayIcon<NetworkTransformChild>();
            DisplayIcon<NetworkTransformVisualizer>();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(1538));

            DisplayIcon<Shadow>();
            DisplayIcon<Outline>();
            DisplayIcon<PositionAsUV1>();

            DisplayIcon<Text>();
            DisplayIcon<Image>();
            DisplayIcon<RawImage>();
            DisplayIcon<Mask>();
            DisplayIcon<RectMask2D>();
            DisplayIcon<Button>();
            DisplayIcon<InputField>();
            DisplayIcon<Toggle>();
            DisplayIcon<ToggleGroup>();
            DisplayIcon<Slider>();
            DisplayIcon<Scrollbar>();
            DisplayIcon<Dropdown>();
            DisplayIcon<ScrollRect>();
            DisplayIcon<Selectable>();
            //DisplayIcon<GUIText>();
            */
            #endregion

        }

        private static void DisplayIcon<T>() where T : Component
        {
            using (new EditorGUILayout.VerticalScope())
            {
                GUILayout.Label(EditorGUIUtility.ObjectContent(null, typeof(T)).image);
                GUILayout.Label(typeof(T).Name);
            }
        }

        void OnGUIHorizontal(ref ShelfItemResponse response, ShelfGUISize size)
        {
            #region Draw upper row
            using (new EditorGUILayout.HorizontalScope(GUILayout.MinHeight(ShelfStyles.ShelfMenuSize)))
            {
                //EditorGUILayout.BeginHorizontal(GUILayout.MinHeight(ShelfStyles.ShelfMenuSize));

                if (GUILayout.Button(ShelfResources.GUICONT_Refresh, ShelfStyles.ShelfIconStyle, GUILayout.MaxWidth(ShelfStyles.ShelfMenuSize)))
                    this.Refresh();

                // draw elements for orientation and size toggle
                if (GUILayout.Button((_guiOrientation == ShelfGUIOrientation.Vertical ? ShelfResources.GUICONT_OrientH : ShelfResources.GUICONT_OrientV),
                                        ShelfStyles.ShelfIconStyle, GUILayout.MaxWidth(ShelfStyles.ShelfMenuSize)))
                    this.ToggleOrientation();

                if (GUILayout.Button((_guiSize == ShelfGUISize.Normal ? ShelfResources.GUICONT_SizeSmall : ShelfResources.GUICONT_SizeNormal),
                                        ShelfStyles.ShelfIconStyle, GUILayout.MaxWidth(ShelfStyles.ShelfMenuSize)))
                    this.ToggleSize();

                // display the search elements
                this.GUISearch();

                Vector2 offset = Vector2.zero;
                _activeShelf = ShelfWindow.DrawOrientedToolbar(ref response, ShelfGUIOrientation.Horizontal, size, _activeShelf, _shelves,
                                                                ref offset, Matrix4x4.identity, ShelfStyles.ShelfStyleNormal, ShelfStyles.ShelfStyleSelected);

                // add the "addshelfcondig" to the vertical GUI!!
                this.GUIAddShelfH();
                this.GUINewShelfH();
                // draw filler toolbar with no fuctionality at all!
                GUILayout.Toolbar(-1, ShelfResources.ShelvesNamesFiller, ShelfStyles.ShelfStyleNormal);

                if (GUILayout.Button("X", ShelfStyles.ShelfStyleNormal, GUILayout.ExpandWidth(false), GUILayout.MinHeight(ShelfStyles.ShelfMenuSize)))
                {
                    this.IsVisible = false;
                    ShelfPrefs.SaveConfig();
                    ShelfPrefs.Refresh();
                }

                //EditorGUILayout.EndHorizontal();
            }
            #endregion

            #region Draw lower row
            using (new EditorGUILayout.HorizontalScope())
            {
                //EditorGUILayout.BeginHorizontal();
                if (_activeShelf >= 0 && _activeShelf < _shelves.Count)
                    _shelves[_activeShelf].DrawItems(ref response, ShelfGUIOrientation.Horizontal, size);
                //EditorGUILayout.EndHorizontal();
            }

            #endregion
        }
        void OnGUIVertical(ref ShelfItemResponse response, ShelfGUISize size)
        {
            //EditorGUILayout.BeginVertical();

            using (new EditorGUILayout.HorizontalScope())
            {

                if (GUILayout.Button(ShelfResources.GUICONT_Refresh, ShelfStyles.ShelfIconStyle, GUILayout.MaxWidth(ShelfStyles.ShelfMenuSize)))
                    this.Refresh();
                // draw elements for orientation and size toggle
                if (GUILayout.Button((_guiOrientation == ShelfGUIOrientation.Vertical ? ShelfResources.GUICONT_OrientH : ShelfResources.GUICONT_OrientV),
                                    ShelfStyles.ShelfIconStyle, GUILayout.MaxWidth(ShelfStyles.ShelfMenuSize)))
                    this.ToggleOrientation();

                if (GUILayout.Button((_guiSize == ShelfGUISize.Normal ? ShelfResources.GUICONT_SizeSmall : ShelfResources.GUICONT_SizeNormal),
                                        ShelfStyles.ShelfIconStyle, GUILayout.MaxWidth(ShelfStyles.ShelfMenuSize)))
                    this.ToggleSize();

                //if (GUILayout.Button(EDITMODE ? ShelfResources.UIRES_EditShelf : ShelfResources.UIRES_Edit, ShelfStyles.ShelfIconStyle, GUILayout.MaxWidth(ShelfStyles.ShelfMenuSize)))
                //    EDITMODE = !EDITMODE;

                // GUILayout.Toolbar(-1, ShelfResources.ShelvesNamesFiller, ShelfStyles.ShelfStyleNormal);
                GUILayout.Label("", ShelfStyles.ShelfStyleNormal);

                if (GUILayout.Button("X", ShelfStyles.ShelfStyleNormal, GUILayout.ExpandWidth(false), GUILayout.MinHeight(ShelfStyles.ShelfMenuSize)))
                {
                    this.IsVisible = false;
                    ShelfPrefs.SaveConfig();
                    ShelfPrefs.Refresh();
                }
            }

            // display the search elements
            this.GUISearch();
            //GUILayout.Toolbar(-1, ShelfResources.ShelvesNamesFiller, ShelfStyles.ShelfStyleNormal, GUILayout.MaxHeight(3.0f), GUILayout.MinHeight(ShelfStyles.ShelfMenuSize));
            //EditorGUILayout.EndVertical();

            Vector2 offset = new Vector2(-(ShelfStyles.ShelfMenuSize * 2) - 1, 0.0f);
            float angle = -90.0f;
            GUIUtility.RotateAroundPivot(angle, new Vector2(0.0f, 0.0f));
            Matrix4x4 rotMatrix = GUI.matrix;

            // draw vertical aligned toolbar
            _activeShelf = ShelfWindow.DrawOrientedToolbar(ref response, ShelfGUIOrientation.Vertical, size, _activeShelf, _shelves,
                                                            ref offset, rotMatrix, ShelfStyles.ShelfStyleNormal, ShelfStyles.ShelfStyleSelected);

            // draw add shelf button in vertical layout
            this.GUIAddShelfV(ref offset, rotMatrix, ShelfStyles.ShelfStyleNormal);
            this.GUINewShelfV(ref offset, rotMatrix, ShelfStyles.ShelfStyleNormal);
            // reset GUI rotation matrix
            GUI.matrix = Matrix4x4.identity;

            using (new EditorGUILayout.HorizontalScope())
            {
                // insert 20px space to prevent overdraw of left vertical toolbar
                GUILayout.Space(ShelfStyles.ShelfMenuSize);
                // begin drawing actual shelf content
                using (new EditorGUILayout.VerticalScope())
                {
                    if (_activeShelf >= 0 && _activeShelf < _shelves.Count)
                        _shelves[_activeShelf].DrawItems(ref response, ShelfGUIOrientation.Vertical, size);
                }
            }
        }

        public static bool DrawVerticalButton(string content, ref Vector2 offset, Matrix4x4 uiTransform, GUIStyle style)
        {
            return DrawVerticalButton(new GUIContent(content), ref offset, uiTransform, style);
        }
        public static bool DrawVerticalButton(GUIContent content, ref Vector2 offset, Matrix4x4 uiTransform, GUIStyle style)
        {
            Vector2 size = style.CalcSize(content);
            float maxWidth = Mathf.Min(size.x, Screen.width);
            //float maxHeight = Mathf.Min(size.y, Screen.height);
            offset.x -= maxWidth;
            // offset.y -= maxWidth;
            GUI.matrix = uiTransform * Matrix4x4.TRS(new Vector3(offset.x, 0, 0), Quaternion.identity, Vector3.one);
            return GUI.Button(new Rect(0, 0, maxWidth, size.y), content, style);
        }
        public static int DrawVerticalToolbar(int selected, GUIContent[] contents, ref Vector2 offset, Matrix4x4 uiTransform, GUIStyle normalStyle, GUIStyle selectedStyle = null)
        {
            for (int i = 0; i < contents.Length; i++)
            {
                if (ShelfWindow.DrawVerticalButton(contents[i], ref offset, uiTransform, (i == selected && selectedStyle != null) ? selectedStyle : normalStyle))
                    return i;
            }
            return selected;
        }
        public static int DrawVerticalToolbar(int selected, List<Shelf> shelves, ref Vector2 offset, Matrix4x4 uiTransform, GUIStyle normalStyle, GUIStyle selectedStyle = null)
        {
            for (int i = 0; i < shelves.Count; i++)
            {
                if (ShelfWindow.DrawVerticalButton(shelves[i].Name, ref offset, uiTransform, (i == selected && selectedStyle != null) ? selectedStyle : normalStyle))
                    return i;
            }
            return selected;
        }

        public static int DrawOrientedToolbar(ref ShelfItemResponse response, ShelfGUIOrientation orientation, ShelfGUISize size,
                                            int selected, List<Shelf> shelves, ref Vector2 offset, Matrix4x4 uiTransform, GUIStyle normalStyle = null, GUIStyle selectedStyle = null)
        {
            int result = selected;
            for (int i = 0; i < shelves.Count; i++)
            {
                bool preTriggerState = response.Triggered;
                ShelfWindow.DrawShelf(ref response, shelves[i], orientation, ref offset, uiTransform, (i == selected) ? selectedStyle : normalStyle);
                //shelves[i].Draw(ref response, orientation, size, (i == selected) ? selectedStyle : normalStyle);
                if (!preTriggerState && response.Triggered)
                    result = i;
            }
            return result;
        }

        public static void DrawShelf(ref ShelfItemResponse response, Shelf shelf, ShelfGUIOrientation orientation,
                                            ref Vector2 offset, Matrix4x4 uiTransform, GUIStyle style)
        {
            bool result = false;
            if (orientation == ShelfGUIOrientation.Horizontal)
                result = shelf.DrawHorizontal(style);
            else
                result = shelf.DrawVertical(ref offset, uiTransform, style);

            if (result && !response.Triggered)
            {
                response.Triggered = true;
                response.Item = shelf;
                response.SetData(Event.current);
            }
        }

        private void GUISearch()
        {
            bool showSearch2 = GUILayout.Button(ShelfResources.GUICONT_Search, ShelfStyles.ShelfIconStyle, GUILayout.MaxWidth(ShelfStyles.ShelfMenuSize), GUILayout.MinHeight(ShelfStyles.ShelfMenuSize));
            if (showSearch2)
            {
                Rect destRect = GUILayoutUtility.GetLastRect();
                // add offset on x-axis when in horizontal layout mode
                if (_guiOrientation == ShelfGUIOrientation.Horizontal)
                    destRect.x += 84.0f + destRect.size.x;
                else
                    destRect.y += ShelfStyles.ShelfMenuSize + destRect.size.y;

                ShelfSearch search = ShelfSearch.DisplayAsDropDown(new Rect(this.position.position + destRect.position, destRect.size), new Vector2(84, 200));
                if (_activeShelf > -1 && _activeShelf < _shelves.Count)
                    search.SetSearchTarget(_shelves[_activeShelf]);
            }
        }

        //private bool GUIToggleSettings(bool value)
        //{
        //    if (_guiOrientation == ShelfGUIOrientation.Horizontal)
        //        return GUILayout.Toggle(value, ShelfResources.GUICONT_Settings, ShelfStyles.ShelfIconStyle, GUILayout.MaxWidth(16.0f), GUILayout.MinHeight(18.0f));
        //    else
        //        return GUILayout.Toggle(value, ShelfResources.GUICONT_Settings, ShelfStyles.ShelfIconStyle, GUILayout.MinHeight(18.0f));
        //}

        private void GUIAddShelfH()
        {
            if (GUILayout.Button(ShelfResources.GUICONT_Add, ShelfStyles.ShelfStyleNormal, GUILayout.ExpandWidth(false), GUILayout.MinHeight(ShelfStyles.ShelfMenuSize)))
            {
                string filePath = EditorUtility.OpenFilePanel("Select Shelf XML configuration file.", "", "xml");
                this.AddConfigFile(filePath);
            }
        }
        private void GUIAddShelfV(ref Vector2 offset, Matrix4x4 uiRotation, GUIStyle style)
        {
            if (ShelfWindow.DrawVerticalButton(ShelfResources.GUICONT_Add, ref offset, uiRotation, style))
            {
                string filePath = EditorUtility.OpenFilePanel("Select Shelf XML configuration file.", "", "xml");
                this.AddConfigFile(filePath);
            }
        }

        private void GUINewShelfH()
        {
            if (GUILayout.Button(ShelfResources.GUICONT_New, ShelfStyles.ShelfStyleNormal, GUILayout.ExpandWidth(false), GUILayout.MinHeight(ShelfStyles.ShelfMenuSize)))
            {
                string ext = ".xml";
                string filePath = EditorUtility.SaveFilePanel("Select destination to save Shelf XML configuration file.", "", "", "xml");
                if (filePath.Equals(string.Empty))
                    return;
                if (!filePath.ToLowerInvariant().EndsWith(ext))
                    filePath += ext;
                if (!filePath.Equals(string.Empty))
                {
                    Debug.Log("New Shelf config: " + filePath);
                    this.CreateConfigFile(filePath);
                    this.AddConfigFile(filePath);
                }
            }
        }
        private void GUINewShelfV(ref Vector2 offset, Matrix4x4 uiRotation, GUIStyle style)
        {
            if (ShelfWindow.DrawVerticalButton(ShelfResources.GUICONT_New, ref offset, uiRotation, style))
            {
                string ext = ".xml";
                string filePath = EditorUtility.SaveFilePanel("Select destination to save Shelf XML configuration file.", "", "MyShelf", "xml");
                if (filePath.Equals(string.Empty))
                    return;
                if (!filePath.ToLowerInvariant().EndsWith(ext))
                    filePath += ext;
                if (!filePath.Equals(string.Empty))
                {
                    Debug.Log("New Shelf config: " + filePath);
                    this.CreateConfigFile(filePath);
                    this.AddConfigFile(filePath);
                }
            }
        }

        private void CreateConfigFile(string filePath)
        {
            XNamespace xNs = "http://tempuri.org/Shelves.xsd";
            XElement elShelves = new XElement(xNs + "Shelves");

            XElement elShelf = new XElement(xNs + "Shelf");
            elShelves.Add(elShelf);
            elShelf.Add(new XAttribute("name", "Shelf"));
            elShelf.Add(new XAttribute("tooltip", "Shelf stub for your custom shelf configuration"));

            XElement elGroup = new XElement(xNs + "Group");
            elShelf.Add(elGroup);
            elGroup.Add(new XAttribute("name", "Group"));
            elGroup.Add(new XAttribute("collapse", "false"));
            elGroup.Add(new XAttribute("tooltip", "Group for your shelf items"));

            // header 1 
            XElement elHeader1 = new XElement(xNs + "Item");
            elGroup.Add(elHeader1);
            elHeader1.Add(new XAttribute("type", "Header"));
            elHeader1.Add(new XAttribute("name", "Header1"));

            // button 1
            XElement elButton1 = new XElement(xNs + "Item");
            elGroup.Add(elButton1);
            elButton1.Add(new XAttribute("type", "Button"));
            elButton1.Add(new XAttribute("name", "Empty Object"));
            elButton1.Add(new XAttribute("tooltip", "Create an empty object inside the scene"));
            elButton1.Add(new XAttribute("iconPath", "Shelves/Icons/GameObjects/Common/Transform.png"));

            XElement elCmd1 = new XElement(xNs + "Command");
            elButton1.Add(elCmd1);
            elCmd1.Add(new XAttribute("type", "MenuItem"));
            elCmd1.Add(new XAttribute("path", "GameObject/Create Empty"));

            // header 2
            XElement elHeader2 = new XElement(xNs + "Item");
            elGroup.Add(elHeader2);
            elHeader2.Add(new XAttribute("type", "Header"));
            elHeader2.Add(new XAttribute("name", "Header2"));

            // button 2
            XElement elButton2 = new XElement(xNs + "Item");
            elGroup.Add(elButton2);
            elButton2.Add(new XAttribute("type", "Button"));
            elButton2.Add(new XAttribute("name", "Camera"));
            elButton2.Add(new XAttribute("tooltip", "Create an new camera in the scene"));
            elButton2.Add(new XAttribute("iconPath", "Shelves/Icons/GameObjects/Common/Camera.png"));

            XElement elCmd2 = new XElement(xNs + "Command");
            elButton2.Add(elCmd2);
            elCmd2.Add(new XAttribute("type", "MenuItem"));
            elCmd2.Add(new XAttribute("path", "GameObject/Camera"));

            try
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
                TextWriter tw = File.CreateText(filePath);
                elShelves.Save(tw);
                tw.Flush();
                tw.Close();
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message + "; " + ex.StackTrace);
            }

        }

        private void AddConfigFile(string filePath)
        {
            if (!filePath.Equals(string.Empty))
            {
                filePath = filePath.Replace(Application.dataPath.Replace("Assets", ""), "");
                if (ShelfWindow.ReadShelfConfig(filePath, ref _shelves))
                {
                    int index = _cfgFiles.Length;

                    for (int i = 0; i < _cfgFiles.Length; i++)
                        Debug.Log(i + ": " + filePath);

                    Array.Resize<string>(ref _cfgFiles, index + 1);
                    _cfgFiles[index] = filePath;

                    this.Repaint();
                }
            }
        }

        private void RemoveShelf(Shelf shelf)
        {
            if (shelf != null
                && _shelves.Contains(shelf))
            {
                Debug.Log("Remove: " + shelf);
                int shelfIndex = _shelves.IndexOf(shelf);
                int cfgFileIndex = Array.IndexOf(_cfgFiles, shelf.SourceFile);
                if (cfgFileIndex != -1)
                    _cfgFiles[cfgFileIndex] = string.Empty;
                _shelves.Remove(shelf);

                if (shelfIndex > _shelves.Count - 1)
                {
                    shelfIndex = _shelves.Count - 1;
                    _activeShelf = shelfIndex;
                }
                this.Refresh();
            }
        }

        private void Refresh()
        {
            this.ReloadShelves();
            this.Repaint();
        }

        private void ToggleSize()
        {
            if (_guiSize == ShelfGUISize.Small)
                this.SwitchSize(ShelfGUISize.Normal);
            else
                this.SwitchSize(ShelfGUISize.Small);
        }
        private void SwitchSize(ShelfGUISize size)
        {
            _guiSize = size;
        }
        private void ToggleOrientation()
        {
            if (_guiOrientation == ShelfGUIOrientation.Vertical)
                this.SwitchOrientation(ShelfGUIOrientation.Horizontal);
            else
                this.SwitchOrientation(ShelfGUIOrientation.Vertical);
        }
        private void SwitchOrientation(ShelfGUIOrientation orientation, bool force = false)
        {
            if (_guiOrientation != orientation
                || force)
            {
                if (_guiOrientation == ShelfGUIOrientation.Horizontal)
                    _prevSizeH = new Vector2(Screen.width, _prevSizeH.y);
                else if (_guiOrientation == ShelfGUIOrientation.Vertical)
                    _prevSizeV = new Vector2(_prevSizeV.x, Screen.height);


                _guiOrientation = orientation;

                //ShelfGUI window = (ShelfGUI)EditorWindow.GetWindow(typeof(ShelfGUI), false, ShelfGUI.WindowTitle);
                // mark the window to have changed orientation
                this._orientationChanged = true;
            }

            if (_guiOrientation == ShelfGUIOrientation.Horizontal)
            {
                this.minSize = new Vector2(_prevSizeH.x, ShelfStyles.ShelfMenuSize + ShelfStyles.ShelfButtonSize[ShelfGUISize.Normal]);
                //this.maxSize = new Vector2(2048.0f, ShelfStyles.ShelfMenuSize + ShelfStyles.ShelfButtonSize[ShelfGUISize.Normal] + 1);
            }
            else
            {
                this.minSize = new Vector2(ShelfStyles.ShelfMenuSize + ShelfStyles.ShelfButtonSize[ShelfGUISize.Normal], _prevSizeV.y);
                //this.maxSize = new Vector2(ShelfStyles.ShelfMenuSize + ShelfStyles.ShelfButtonSize[ShelfGUISize.Normal] + 1, 2048.0f);
            }

        }

        private static bool ReadShelfConfig(string xmlPath, ref List<Shelf> shelves)
        {
            if (File.Exists(xmlPath))
            {
                try
                {
                    XDocument xDoc = XDocument.Load(xmlPath);
                    XNamespace xNs = "http://tempuri.org/Shelves.xsd";

                    // iterate over all shelf nodes in the xml document
                    foreach (XElement elShelf in xDoc.Root.Elements(xNs + "Shelf"))
                    {
                        XAttribute attrName = elShelf.Attribute("name");
                        XAttribute attrTooltip = elShelf.Attribute("tooltip");

                        // create shelf container object
                        Shelf shelf = new Shelf((attrName != null ? attrName.Value : "Shelf" + shelves.Count), (attrTooltip != null ? attrTooltip.Value : string.Empty));
                        shelf.SourceFile = xmlPath;
                        if (FindShelfByName(shelves, shelf.Name) != null)
                            continue;

                        // add shelf item range to the shelf itself
                        shelf.AddRange(ShelfUtility.ReadShelfItems(elShelf, xNs));

                        shelves.Add(shelf);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    // print exception to console if any occured
                    Debug.LogWarning("Failed to read the xml configuration: " + ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
            return false;
        }
        private static Shelf FindShelfByName(List<Shelf> shelves, string name)
        {
            return shelves.Find(x => x.Name.Equals(name));
        }

        public static void FilterShelves(List<Shelf> shelves, string filter, List<ShelfItem> results, bool clear = true)
        {
            if (shelves == null)
                return;
            if (clear)
                results.Clear();
            foreach (Shelf shelf in shelves)
            {
                ShelfWindow.FilterShelfItems(shelf.Items, filter, results);
            }

        }
        public static void FilterShelves(Shelf shelf, string filter, List<ShelfItem> results, bool clear = true)
        {
            if (shelf == null)
                return;
            if (clear)
                results.Clear();
            ShelfWindow.FilterShelfItems(shelf.Items, filter, results);
        }
        public static void FilterShelfItems(List<ShelfItem> items, string filter, List<ShelfItem> results, bool clear)
        {
            if (items == null)
                return;
            if (clear)
                results.Clear();
            ShelfWindow.FilterShelfItems(items, filter, results);
        }
        private static void FilterShelfItems(List<ShelfItem> items, string filter, List<ShelfItem> results)
        {
            foreach (ShelfButton button in items.FindAll(x => x.GetType().Equals(typeof(ShelfButton))))
                if (Regex.IsMatch(button.Name, filter, RegexOptions.IgnoreCase))
                    results.Add(button);
            foreach (ShelfGroup group in items.FindAll(x => x.GetType().Equals(typeof(ShelfGroup))))
            {
                // add a new ShelfHeader (just for display purposes) and add all childs of a group

                //if (group.Name.Contains(filter))
                if (Regex.IsMatch(group.Name, filter, RegexOptions.IgnoreCase))
                {
                    results.Add(group);
                }
                else
                    ShelfWindow.FilterShelfItems(group.Items, filter, results);
            }
        }

    }


}